//#define UNICODE
#include <Windows.h>
#include <stdio.h>
#include <strsafe.h>
#include <tchar.h>
#include "Win32SDKDialog.h"
#include "Receipt.h"

int WINAPI WinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR inCmdLine,
	int nCmdShow)
{
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR AppName[] = TEXT("Window");

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpszClassName = AppName;
	wndclass.lpszMenuName = NULL;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndclass.hIcon = LoadIcon(hInstance, TEXT("IDI_ICON"));
	wndclass.hIconSm = LoadIcon(hInstance, TEXT("IDI_ICON"));
	wndclass.hCursor = LoadCursor(hInstance, TEXT("IDI_ICON"));

	RegisterClassEx(&wndclass);

	hwnd = CreateWindow(AppName,
		TEXT("First Window"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0, //CW_USEDEFAULT,
		0, //CW_USEDEFAULT,
		WINWIDTH, // CW_USEDEFAULT,
		WINHEIGHT, // CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);
	gHwnd = hwnd;
	if (hwnd == NULL)
	{
		MessageBox(hwnd, TEXT("Window not created"), TEXT("Error.."), MB_OK);
		exit(0);
	}

	ShowWindow(hwnd, SW_MAXIMIZE);
	UpdateWindow(hwnd);

	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return ((int)msg.wParam);
}


LRESULT CALLBACK WndProc(HWND hwnd,
	UINT iMsg,
	WPARAM wParam,
	LPARAM lParam)
{

	bool paintMainWindow(void);


	switch (iMsg)
	{
	
	case WM_CREATE:
		//DialogBox(hInst, TEXT("DATAENTRY"), hwnd, MyDialogProc);
		break;

	case WM_SIZE:
	    InvalidateRect(hwnd, NULL, FALSE);
		break;

	case WM_PAINT:
		paintMainWindow();		
		break;
		
	//Dialogue Destroy notification to main/parent window
	/*case WM_PARENTNOTIFY:
		if (LOWORD(wParam) == 0x0002) {
			splashScreenActive = false;
		}
		break;*/
	
	case WM_DESTROY:		
		PostQuitMessage(0);
		break;

	case WM_KEYDOWN:
		switch (wParam) {
		case VK_SPACE:
			hInst = (HINSTANCE)GetWindowLong(hwnd, GWLP_HINSTANCE);
			DialogBox(hInst, TEXT("DATAENTRY"), hwnd, MyDialogProc);
			break;

		case VK_ESCAPE:
			if (!splashScreenActive) {
				hInst = (HINSTANCE)GetWindowLong(hwnd, GWLP_HINSTANCE);
				DialogBox(hInst, TEXT("THANKYOU"), hwnd, MyThankyouProc);
			}
			PostQuitMessage(0);
			break;
			
		case 'P':
			hInst = (HINSTANCE)GetWindowLong(hwnd, GWLP_HINSTANCE);
			DialogBox(hInst, TEXT("RECEIPT"), hwnd, MyReceiptDialogProc);
			break;

		default:
			break;
		}
		break;

	default:
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}



BOOL CALLBACK MyThankyouProc(HWND hDlg, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	switch (iMsg)
	{
	case WM_INITDIALOG:
		return(TRUE);

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDOK:
			EndDialog(hDlg, 0);
			break;

		}
		return TRUE;
	}

	return FALSE;

}

BOOL CALLBACK MyDialogProc(HWND hDlg, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	bool initializeDialogBox(void);
	void addItemsToCombobox(TCHAR **, int, int);

	HDC hdcInst, hdcBitmap;
	PAINTSTRUCT ps;
	BITMAP bp;
	RECT r;
	HINSTANCE hInst;
	HBITMAP hBitmap; //bitmap object to hold your bitmap
	switch (iMsg)
	{
	case WM_INITDIALOG:
		
		ghDlg = hDlg;
		SetFocus(GetDlgItem(hDlg, ID_CBCPUCOMPANY));
		//fill all combo-boxes with data
		initializeDialogBox();	
		return(TRUE);

	case WM_PAINT:
		hdcInst = BeginPaint(hDlg, &ps);
		hInst = (HINSTANCE)GetWindowLong(hDlg, GWLP_HINSTANCE);
		hBitmap = LoadBitmap(hInst, MAKEINTRESOURCE(IDB_BGID));
		hdcBitmap = CreateCompatibleDC(hdcInst);
		GetClientRect(hDlg, &r);
		SelectObject(hdcBitmap, hBitmap);
		GetObject(hBitmap, sizeof(bp), &bp);
		BitBlt(hdcInst, 0, 0, r.right - r.left, r.bottom - r.top, hdcBitmap, 0, 0, SRCCOPY);
		DeleteDC(hdcBitmap);
		EndPaint(hDlg, &ps);
		break;

	case WM_CTLCOLORSTATIC:
		hdcInst = (HDC)wParam;
		SetTextColor(hdcInst, RGB(255, 255, 255));
		SetBkColor(hdcInst, TRANSPARENT);
		SetBkMode(hdcInst, TRANSPARENT);
		return (LRESULT)GetStockObject(NULL_BRUSH);
	
	case WM_COMMAND:

		switch (LOWORD(wParam))
		{
		
			case IDOK:
				//Here change the size of the window for further painting of material list
				GetWindowPlacement(gHwnd, &wpPrev);
				wpPrev.rcNormalPosition.top = wpPrev.rcNormalPosition.top + 0x50;
				wpPrev.rcNormalPosition.bottom = wpPrev.rcNormalPosition.bottom - 0x50;
				wpPrev.rcNormalPosition.left = wpPrev.rcNormalPosition.left + 0x50;
				wpPrev.rcNormalPosition.right = wpPrev.rcNormalPosition.right - 0x50;
				SetWindowPlacement(gHwnd, &wpPrev);
				splashScreenActive = false;
				InvalidateRect(gHwnd, NULL, true);
				EndDialog(hDlg, 0);
				break;
			case IDCANCEL:
				EndDialog(hDlg, 0);
				break;
			default:
				break;
		}
	
	
		switch (HIWORD(wParam))
		{
		case CBN_SELCHANGE:
			switch (LOWORD(wParam))
			{

			case ID_CBCPUCOMPANY:

				giSelect = SendDlgItemMessage(hDlg, ID_CBCPUCOMPANY, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

				//GetDia
				switch (giSelect) {
				case 0:
					ShowWindow(GetDlgItem(ghDlg, ID_CBCPUPROCESSOR), SW_HIDE);
					ShowWindow(GetDlgItem(ghDlg, ID_CBCPUMODEL), SW_HIDE);
					ShowWindow(GetDlgItem(ghDlg, ID_CBCPUGENERATION), SW_HIDE);
					gMaterialList.cpuCompany = NULL;
					gMaterialList.cpuProcessor = NULL;
					gMaterialList.cpuModel = NULL;
					gMaterialList.cpuGeneration = NULL;
					break;

				case 1:
					SendDlgItemMessage(hDlg, ID_CBCPUPROCESSOR, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
					addItemsToCombobox(intelProcessor, ID_CBCPUPROCESSOR, sizeof(intelProcessor) / SIZE_OF_POINTER);
					ShowWindow(GetDlgItem(ghDlg, ID_CBCPUPROCESSOR), SW_SHOW);
					ShowWindow(GetDlgItem(ghDlg, ID_CBCPUMODEL), SW_HIDE);
					ShowWindow(GetDlgItem(ghDlg, ID_CBCPUGENERATION), SW_HIDE);
					gMaterialList.cpuCompany = cpuCompany[giSelect];
					break;

				case 2:
					SendDlgItemMessage(hDlg, ID_CBCPUPROCESSOR, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
					addItemsToCombobox(AMDProcessor, ID_CBCPUPROCESSOR, sizeof(AMDProcessor) / SIZE_OF_POINTER);
					ShowWindow(GetDlgItem(ghDlg, ID_CBCPUPROCESSOR), SW_SHOW);
					ShowWindow(GetDlgItem(ghDlg, ID_CBCPUMODEL), SW_HIDE);
					ShowWindow(GetDlgItem(ghDlg, ID_CBCPUGENERATION), SW_HIDE);
					gMaterialList.cpuCompany = cpuCompany[giSelect];
					break;

				}
				break;

			case ID_CBCPUPROCESSOR:

				giSelect = SendDlgItemMessage(hDlg, ID_CBCPUPROCESSOR, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

				if (!wcscmp(gMaterialList.cpuCompany, TEXT("Intel"))) {

					switch (giSelect) {
					case 0:
						ShowWindow(GetDlgItem(ghDlg, ID_CBCPUMODEL), SW_HIDE);
						ShowWindow(GetDlgItem(ghDlg, ID_CBCPUGENERATION), SW_HIDE);
						gMaterialList.cpuProcessor = NULL;
						gMaterialList.cpuModel = NULL;
						gMaterialList.cpuGeneration = NULL;
						prices[0] = 0;
						break;

					case 1:
						SendDlgItemMessage(hDlg, ID_CBCPUMODEL, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
						addItemsToCombobox(intelCoreI5Models, ID_CBCPUMODEL, sizeof(intelCoreI5Models) / SIZE_OF_POINTER);
						ShowWindow(GetDlgItem(ghDlg, ID_CBCPUMODEL), SW_SHOW);
						ShowWindow(GetDlgItem(ghDlg, ID_CBCPUGENERATION), SW_HIDE);
						gMaterialList.cpuProcessor = intelProcessor[giSelect];
						prices[0] = 14000;
						break;

					case 2:
						SendDlgItemMessage(hDlg, ID_CBCPUMODEL, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
						addItemsToCombobox(intelCoreI7Models, ID_CBCPUMODEL, sizeof(intelCoreI7Models) / SIZE_OF_POINTER);
						ShowWindow(GetDlgItem(ghDlg, ID_CBCPUMODEL), SW_SHOW);
						ShowWindow(GetDlgItem(ghDlg, ID_CBCPUGENERATION), SW_HIDE);
						gMaterialList.cpuProcessor = intelProcessor[giSelect];
						prices[0] = 22500;
						break;
					}
				}
				else if (!wcscmp(gMaterialList.cpuCompany, TEXT("AMD")))
				{
					switch (giSelect) {
					case 0:
						ShowWindow(GetDlgItem(ghDlg, ID_CBCPUMODEL), SW_HIDE);
						ShowWindow(GetDlgItem(ghDlg, ID_CBCPUGENERATION), SW_HIDE);
						gMaterialList.cpuProcessor = NULL;
						gMaterialList.cpuModel = NULL;
						gMaterialList.cpuGeneration = NULL;
						break;

					case 1:
						SendDlgItemMessage(hDlg, ID_CBCPUMODEL, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
						addItemsToCombobox(AMDRyzenModels, ID_CBCPUMODEL, sizeof(AMDRyzenModels) / SIZE_OF_POINTER);
						ShowWindow(GetDlgItem(ghDlg, ID_CBCPUMODEL), SW_SHOW);
						ShowWindow(GetDlgItem(ghDlg, ID_CBCPUGENERATION), SW_HIDE);
						gMaterialList.cpuProcessor = AMDProcessor[giSelect];
						prices[0] = 12500;
						break;

					case 2:
						SendDlgItemMessage(hDlg, ID_CBCPUMODEL, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
						addItemsToCombobox(AMDSempronModels, ID_CBCPUMODEL, sizeof(AMDSempronModels) / SIZE_OF_POINTER);
						ShowWindow(GetDlgItem(ghDlg, ID_CBCPUMODEL), SW_SHOW);
						ShowWindow(GetDlgItem(ghDlg, ID_CBCPUGENERATION), SW_HIDE);
						gMaterialList.cpuProcessor = AMDProcessor[giSelect];
						prices[0] = 16000;
						break;
					}

				}
				break;

			case ID_CBCPUMODEL:

				giSelect = SendDlgItemMessage(hDlg, ID_CBCPUMODEL, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

				if (giSelect == 0)
				{
					ShowWindow(GetDlgItem(ghDlg, ID_CBCPUGENERATION), SW_HIDE);
					gMaterialList.cpuModel = NULL;
					gMaterialList.cpuGeneration = NULL;
					//break;
				}
				else
				{
					SendDlgItemMessage(hDlg, ID_CBCPUGENERATION, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
					if (!wcscmp(gMaterialList.cpuCompany, TEXT("Intel")))
					{
						addItemsToCombobox(intelCpuGenration, ID_CBCPUGENERATION, sizeof(intelCpuGenration) / SIZE_OF_POINTER);
						if (!wcscmp(gMaterialList.cpuProcessor, TEXT("Core-i7")))
							gMaterialList.cpuModel = intelCoreI7Models[giSelect];
						else
							gMaterialList.cpuModel = intelCoreI5Models[giSelect];

					}
					else if (!wcscmp(gMaterialList.cpuCompany, TEXT("AMD")))
					{
						addItemsToCombobox(AMDCpuGenration, ID_CBCPUGENERATION, sizeof(AMDCpuGenration) / SIZE_OF_POINTER);
						if (!wcscmp(gMaterialList.cpuProcessor, TEXT("Ryzen")))
							gMaterialList.cpuModel = AMDRyzenModels[giSelect];
						else
							gMaterialList.cpuModel = AMDSempronModels[giSelect];
					}

					ShowWindow(GetDlgItem(ghDlg, ID_CBCPUGENERATION), SW_SHOW);
				}
				break;


			case ID_CBCPUGENERATION:

				giSelect = SendDlgItemMessage(ghDlg, ID_CBCPUGENERATION, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

				if (giSelect != 0)
				{
					if (!wcscmp(gMaterialList.cpuCompany, TEXT("Intel")))
					{
						gMaterialList.cpuGeneration = intelCpuGenration[giSelect];
					}
					else if (!wcscmp(gMaterialList.cpuCompany, TEXT("AMD")))
					{
						gMaterialList.cpuGeneration = AMDCpuGenration[giSelect];
					}

				}

				break;


			case ID_CBRAMCOMPANY:
				giSelect = SendDlgItemMessage(ghDlg, ID_CBRAMCOMPANY, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

				if (giSelect != 0) {
					SendDlgItemMessage(hDlg, ID_CBRAMSIZE, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
					ShowWindow(GetDlgItem(ghDlg, ID_CBRAMSIZE), SW_SHOW);
					addItemsToCombobox(RAMSize, ID_CBRAMSIZE, sizeof(RAMSize) / SIZE_OF_POINTER);
					gMaterialList.ramCompany = RAMCompany[giSelect];
					prices[1] = 6500;
				}
				else
				{
					ShowWindow(GetDlgItem(ghDlg, ID_CBRAMSIZE), SW_HIDE);
				}

				break;

			case ID_CBRAMSIZE:
				giSelect = SendDlgItemMessage(ghDlg, ID_CBRAMSIZE, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);
				if (giSelect != 0) {
					gMaterialList.ramSize = RAMSize[giSelect];
					if (RAMSize[giSelect] == TEXT("8GB"))
						prices[1] = 6500;
					else
						prices[1] = 12200;
				}

				break;

			case ID_CBMOTHERBOSRDCOMPANY:
				giSelect = SendDlgItemMessage(ghDlg, ID_CBMOTHERBOSRDCOMPANY, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);
				switch (giSelect) {
				case 0:
					ShowWindow(GetDlgItem(ghDlg, ID_CBMOTHERBOSRD), SW_HIDE);
					gMaterialList.monitorCompany = NULL;
					gMaterialList.motherBoard = NULL;
					prices[2] = 0;
 					break;

				case 1:
					SendDlgItemMessage(hDlg, ID_CBMOTHERBOSRD, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
					addItemsToCombobox(motherBoardAsus, ID_CBMOTHERBOSRD, sizeof(motherBoardAsus) / SIZE_OF_POINTER);
					ShowWindow(GetDlgItem(ghDlg, ID_CBMOTHERBOSRD), SW_SHOW);
					gMaterialList.motherBoardCompany = motherBoardCompany[giSelect];
					gMaterialList.motherBoard = motherBoardAsus[1];
					prices[2] = 6700;
					break;

				case 2:
					SendDlgItemMessage(hDlg, ID_CBMOTHERBOSRD, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
					addItemsToCombobox(motherBoardGigabyte, ID_CBMOTHERBOSRD, sizeof(motherBoardGigabyte) / SIZE_OF_POINTER);
					ShowWindow(GetDlgItem(ghDlg, ID_CBMOTHERBOSRD), SW_SHOW);
					gMaterialList.motherBoardCompany = motherBoardCompany[giSelect];
					gMaterialList.motherBoard = motherBoardGigabyte[1];
					prices[2] = 7100;
					break;

				case 3:
					SendDlgItemMessage(hDlg, ID_CBMOTHERBOSRD, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
					addItemsToCombobox(motherBoardBiostar, ID_CBMOTHERBOSRD, sizeof(motherBoardBiostar) / SIZE_OF_POINTER);
					ShowWindow(GetDlgItem(ghDlg, ID_CBMOTHERBOSRD), SW_SHOW);
					gMaterialList.motherBoardCompany = motherBoardCompany[giSelect];
					gMaterialList.motherBoard = motherBoardBiostar[1];
					prices[2] = 8520;
					break;

				}
				break;
				

			case ID_CBMOTHERBOSRD:
				giSelect = SendDlgItemMessage(ghDlg, ID_CBMOTHERBOSRD, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);
				if (!wcscmp(gMaterialList.motherBoardCompany, motherBoardCompany[1])) { //ASUS

					switch (giSelect) {
					case 0:
						gMaterialList.motherBoard = motherBoardAsus[1];
						prices[2] = 6700;
						break;

					case 1:
						gMaterialList.motherBoard = motherBoardAsus[giSelect];
						prices[2] = 6700;
						break;

					case 2:
						gMaterialList.motherBoard = motherBoardAsus[giSelect];
						prices[2] = 7000;
						break;

					case 3:
						gMaterialList.motherBoard = motherBoardAsus[giSelect];
						prices[2] = 6350;
						break;
					}
				}
				else if (!wcscmp(gMaterialList.motherBoardCompany, motherBoardCompany[2]))
				{
					switch (giSelect) {
					case 0:
						gMaterialList.motherBoard = motherBoardGigabyte[1];
						prices[2] = 7100;
						break;

					case 1:
						gMaterialList.motherBoard = motherBoardGigabyte[giSelect];
						prices[2] = 7100;
						break;

					case 2:
						gMaterialList.motherBoard = motherBoardGigabyte[giSelect];
						prices[2] = 7450;
						break;

					case 3:
						gMaterialList.motherBoard = motherBoardGigabyte[giSelect];
						prices[2] = 7960;
						break;
					}

				}
				else if (!wcscmp(gMaterialList.motherBoardCompany, motherBoardCompany[3]))
				{
					switch (giSelect) {
					case 0:
						gMaterialList.motherBoard = motherBoardBiostar[1];
						prices[2] = 8520;
						break;

					case 1:
						gMaterialList.motherBoard = motherBoardBiostar[giSelect];
						prices[2] = 8520;
						break;

					case 2:
						gMaterialList.motherBoard = motherBoardBiostar[giSelect];
						prices[2] = 8800;
						break;

					case 3:
						gMaterialList.motherBoard = motherBoardBiostar[giSelect];
						prices[2] = 9050;
						break;
					}

				}
				break;

			case ID_CBGCCOMPANY:
				giSelect = SendDlgItemMessage(hDlg, ID_CBGCCOMPANY, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);
				ShowWindow(GetDlgItem(ghDlg, ID_CBGCSERIAL1), SW_HIDE);
				ShowWindow(GetDlgItem(ghDlg, ID_CBGCSIZE), SW_HIDE);
				switch (giSelect)
				{
				case 0:
					gMaterialList.gcCompany = NULL;
					gMaterialList.gcSerial = NULL;
					gMaterialList.gcSize = NULL;
					break;

				case 1:
					SendDlgItemMessage(hDlg, ID_CBGCSERIAL1, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
					addItemsToCombobox(graphicsIntelSerial, ID_CBGCSERIAL1, sizeof(graphicsIntelSerial) / SIZE_OF_POINTER);
					ShowWindow(GetDlgItem(ghDlg, ID_CBGCSERIAL1), SW_SHOW);
					gMaterialList.gcCompany = graphicsCardCompany[giSelect];
					gMaterialList.gcSize = NULL;
					break;

				case 2:
					SendDlgItemMessage(hDlg, ID_CBGCSERIAL1, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
					addItemsToCombobox(graphicsAMDSerial, ID_CBGCSERIAL1, sizeof(graphicsAMDSerial) / SIZE_OF_POINTER);
					ShowWindow(GetDlgItem(ghDlg, ID_CBGCSERIAL1), SW_SHOW);
					gMaterialList.gcCompany = graphicsCardCompany[giSelect];
					gMaterialList.gcSize = NULL;
					break;


				case 3:
					SendDlgItemMessage(hDlg, ID_CBGCSERIAL1, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
					addItemsToCombobox(graphicsNVIDIASerial, ID_CBGCSERIAL1, sizeof(graphicsNVIDIASerial) / SIZE_OF_POINTER);
					ShowWindow(GetDlgItem(ghDlg, ID_CBGCSERIAL1), SW_SHOW);
					gMaterialList.gcCompany = graphicsCardCompany[giSelect];
					gMaterialList.gcSize = NULL;
					break;
				}
				break;

			case ID_CBGCSERIAL1:
				giSelect = SendDlgItemMessage(hDlg, ID_CBGCSERIAL1, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);
				if (giSelect != 0)
				{
					SendDlgItemMessage(hDlg, ID_CBGCSIZE, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
					addItemsToCombobox(graphicsCardSize, ID_CBGCSIZE, sizeof(graphicsCardSize) / SIZE_OF_POINTER);
					ShowWindow(GetDlgItem(ghDlg, ID_CBGCSIZE), SW_SHOW);

					if (!wcscmp(gMaterialList.gcCompany, TEXT("Intel"))) {
						gMaterialList.gcSerial = graphicsIntelSerial[giSelect];
						prices[3] = 7000;
					}
					else if (!wcscmp(gMaterialList.gcCompany, TEXT("AMD"))) {
						gMaterialList.gcSerial = graphicsAMDSerial[giSelect];
						prices[3] = 8800;
					}
					else {
						gMaterialList.gcSerial = graphicsNVIDIASerial[giSelect];
						prices[3] = 12800;
					}
					gMaterialList.gcSize = graphicsCardSize[1]; //default size
				}
				break;

			case ID_CBGCSIZE:
				giSelect = SendDlgItemMessage(hDlg, ID_CBGCSIZE, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

				if (giSelect != 0)
				{
					gMaterialList.gcSize = graphicsCardSize[giSelect];
				}
				break;

			case ID_CBHDCOMPANY:
				giSelect = SendDlgItemMessage(ghDlg, ID_CBHDCOMPANY, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

				if (giSelect != 0) {
					SendDlgItemMessage(hDlg, ID_CBHDSIZE, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
					ShowWindow(GetDlgItem(ghDlg, ID_CBHDSIZE), SW_SHOW);
					addItemsToCombobox(hardDiskSize, ID_CBHDSIZE, sizeof(hardDiskSize) / SIZE_OF_POINTER);
					gMaterialList.hdCompany = hardDiskSize[giSelect];
					if (giSelect == 1)
						prices[4] = 3127;
					else
						prices[4] = 2898;
				}
				else
				{
					ShowWindow(GetDlgItem(ghDlg, ID_CBHDSIZE), SW_HIDE);
				}

				break;

			case ID_CBHDSIZE:
				giSelect = SendDlgItemMessage(ghDlg, ID_CBHDSIZE, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

				if (giSelect != 0)
					gMaterialList.hdSize = hardDiskSize[giSelect];
				break;

			case ID_CBCDDVDDRIVECOMPANY:
				giSelect = SendDlgItemMessage(ghDlg, ID_CBCDDVDDRIVECOMPANY, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

				switch (giSelect) {
				case 0:
					ShowWindow(GetDlgItem(ghDlg, ID_CBCDDVDDRIVE), SW_HIDE);
					gMaterialList.cdDvdDriveCompany = NULL;
					gMaterialList.cdDvdDrive = NULL;
					prices[5] = 0;
					break;

				case 1:
					SendDlgItemMessage(hDlg, ID_CBCDDVDDRIVE, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
					addItemsToCombobox(CDDVDDriveAMW, ID_CBCDDVDDRIVE, sizeof(CDDVDDriveAMW) / SIZE_OF_POINTER);
					ShowWindow(GetDlgItem(ghDlg, ID_CBCDDVDDRIVE), SW_SHOW);
					gMaterialList.cdDvdDriveCompany = CDDVDDriveCompany[giSelect];
					gMaterialList.cdDvdDrive = CDDVDDriveAMW[1];
					prices[5] = 2400;
					break;

				case 2:
					SendDlgItemMessage(hDlg, ID_CBCDDVDDRIVE, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
					addItemsToCombobox(CDDVDDriveAstrad, ID_CBCDDVDDRIVE, sizeof(CDDVDDriveAstrad) / SIZE_OF_POINTER);
					ShowWindow(GetDlgItem(ghDlg, ID_CBCDDVDDRIVE), SW_SHOW);
					gMaterialList.cdDvdDriveCompany = CDDVDDriveCompany[giSelect];
					gMaterialList.cdDvdDrive = CDDVDDriveAstrad[1];
					prices[5] = 2650;
					break;

				case 3:
					SendDlgItemMessage(hDlg, ID_CBCDDVDDRIVE, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
					addItemsToCombobox(CDDVDDriveAcros, ID_CBCDDVDDRIVE, sizeof(CDDVDDriveAcros) / SIZE_OF_POINTER);
					ShowWindow(GetDlgItem(ghDlg, ID_CBCDDVDDRIVE), SW_SHOW);
					gMaterialList.cdDvdDriveCompany = CDDVDDriveCompany[giSelect];
					gMaterialList.cdDvdDrive = CDDVDDriveAcros[1];
					prices[5] = 1800;
					break;

				}
				break;

			case ID_CBCDDVDDRIVE:
				giSelect = SendDlgItemMessage(ghDlg, ID_CBCDDVDDRIVE, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

				if (!wcscmp(gMaterialList.cdDvdDriveCompany, CDDVDDriveCompany[1])) { //AMW

					switch (giSelect) {
					case 0:
						gMaterialList.cdDvdDrive = CDDVDDriveAMW[1];
						prices[5] = 2400;
						break;

					case 1:
						gMaterialList.cdDvdDrive = CDDVDDriveAMW[giSelect];
						prices[5] = 2100;
						break;

					case 2:
						gMaterialList.cdDvdDrive = CDDVDDriveAMW[giSelect];
						prices[5] = 1980;
						break;

					case 3:
						gMaterialList.cdDvdDrive = CDDVDDriveAMW[giSelect];
						prices[5] = 2740;
						break;
					}
				}
				else if (!wcscmp(gMaterialList.cdDvdDriveCompany, CDDVDDriveCompany[2]))
				{
					switch (giSelect) {
					case 0:
						gMaterialList.cdDvdDrive = CDDVDDriveAstrad[1];
						prices[5] = 2650;
						break;

					case 1:
						gMaterialList.cdDvdDrive = CDDVDDriveAstrad[giSelect];
						prices[5] = 2650;
						break;

					case 2:
						gMaterialList.cdDvdDrive = CDDVDDriveAstrad[giSelect];
						prices[5] = 2900;
						break;

					case 3:
						gMaterialList.cdDvdDrive = CDDVDDriveAstrad[giSelect];
						prices[5] = 3550;
						break;

					case 4:
						gMaterialList.cdDvdDrive = CDDVDDriveAstrad[giSelect];
						prices[5] = 3200;
						break;
					}

				}
				else if (!wcscmp(gMaterialList.cdDvdDriveCompany, CDDVDDriveCompany[3]))
				{
					switch (giSelect) {
					case 0:
						gMaterialList.cdDvdDrive = CDDVDDriveAcros[1];
						prices[5] = 1800;
						break;

					case 1:
						gMaterialList.cdDvdDrive = CDDVDDriveAcros[giSelect];
						prices[5] = 2510;
						break;

					case 2:
						gMaterialList.cdDvdDrive = CDDVDDriveAcros[giSelect];
						prices[5] = 3000;
						break;

					case 3:
						gMaterialList.cdDvdDrive = CDDVDDriveAcros[giSelect];
						prices[5] = 2460;
						break;
					}

				}
				break;

			case ID_CBSMPSCOMPANY:
				giSelect = SendDlgItemMessage(ghDlg, ID_CBSMPSCOMPANY, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

				switch (giSelect) {
				case 0:
					ShowWindow(GetDlgItem(ghDlg, ID_CBSMPS), SW_HIDE);
					ShowWindow(GetDlgItem(ghDlg, ID_CBSMPSPOWER), SW_HIDE);
					gMaterialList.smpsCompany = NULL;
					gMaterialList.smps = NULL;
					gMaterialList.smpsPower = NULL;
					prices[6] = 0;
					break;

				case 1:
					SendDlgItemMessage(hDlg, ID_CBSMPS, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
					addItemsToCombobox(SMPSCorsair, ID_CBSMPS, sizeof(SMPSCorsair) / SIZE_OF_POINTER);
					ShowWindow(GetDlgItem(ghDlg, ID_CBSMPS), SW_SHOW);
					ShowWindow(GetDlgItem(ghDlg, ID_CBSMPSPOWER), SW_HIDE);
					gMaterialList.smpsCompany = SMPSCompany[giSelect];
					gMaterialList.smps = SMPSCorsair[1];
					gMaterialList.smpsPower = SMPSPower[1];
					prices[6] = 2300;
					break;

				case 2:
					SendDlgItemMessage(hDlg, ID_CBSMPS, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
					addItemsToCombobox(SMPSAntec, ID_CBSMPS, sizeof(SMPSAntec) / SIZE_OF_POINTER);
					ShowWindow(GetDlgItem(ghDlg, ID_CBSMPS), SW_SHOW);
					ShowWindow(GetDlgItem(ghDlg, ID_CBSMPSPOWER), SW_HIDE);
					gMaterialList.smpsCompany = SMPSCompany[giSelect];
					gMaterialList.smps = SMPSAntec[1];
					gMaterialList.smpsPower = SMPSPower[1];
					prices[6] = 2400;
					break;

				case 3:
					SendDlgItemMessage(hDlg, ID_CBSMPS, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
					addItemsToCombobox(SMPSSea, ID_CBSMPS, sizeof(SMPSSea) / SIZE_OF_POINTER);
					ShowWindow(GetDlgItem(ghDlg, ID_CBSMPS), SW_SHOW);
					ShowWindow(GetDlgItem(ghDlg, ID_CBSMPSPOWER), SW_HIDE);
					gMaterialList.smpsCompany = SMPSCompany[giSelect];
					gMaterialList.smps = SMPSSea[1];
					gMaterialList.smpsPower = SMPSPower[1];
					prices[6] = 2500;
					break;

				}
				break;

			case ID_CBSMPS:
				giSelect = SendDlgItemMessage(ghDlg, ID_CBSMPS, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

				if (!wcscmp(gMaterialList.smpsCompany, SMPSCompany[1])) {

					if (giSelect != 0) {
						SendDlgItemMessage(hDlg, ID_CBSMPSPOWER, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
						addItemsToCombobox(SMPSPower, ID_CBSMPSPOWER, sizeof(SMPSPower) / SIZE_OF_POINTER);
						ShowWindow(GetDlgItem(ghDlg, ID_CBSMPSPOWER), SW_SHOW);
						gMaterialList.smps = SMPSCorsair[giSelect];
						if (giSelect == 1)
							prices[6] = 2300;
						else if (giSelect == 2)
							prices[6] = 2100;
						else
							prices[6] = 1900;
					}
					else {

						ShowWindow(GetDlgItem(ghDlg, ID_CBSMPSPOWER), SW_HIDE);
						gMaterialList.smps = SMPSCorsair[1];
						prices[6] = 2300;
					}
	
				}
				else if (!wcscmp(gMaterialList.smpsCompany, SMPSCompany[2]))
				{
					if (giSelect != 0) {
						SendDlgItemMessage(hDlg, ID_CBSMPSPOWER, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
						addItemsToCombobox(SMPSPower, ID_CBSMPSPOWER, sizeof(SMPSPower) / SIZE_OF_POINTER);
						ShowWindow(GetDlgItem(ghDlg, ID_CBSMPSPOWER), SW_SHOW);
						gMaterialList.smps = SMPSAntec[giSelect];
						if (giSelect == 1)
							prices[6] = 2400;
						else if (giSelect == 2)
							prices[6] = 2200;
						else
							prices[6] = 2000;
					}
					else {

						ShowWindow(GetDlgItem(ghDlg, ID_CBSMPSPOWER), SW_HIDE);
						gMaterialList.smps = SMPSAntec[1];
						prices[6] = 2400;
					}

				}
				else if (!wcscmp(gMaterialList.smpsCompany, SMPSCompany[3]))
				{
					if (giSelect != 0) {
						SendDlgItemMessage(hDlg, ID_CBSMPSPOWER, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
						addItemsToCombobox(SMPSPower, ID_CBSMPSPOWER, sizeof(SMPSPower) / SIZE_OF_POINTER);
						ShowWindow(GetDlgItem(ghDlg, ID_CBSMPSPOWER), SW_SHOW);
						gMaterialList.smps = SMPSSea[giSelect];
						if (giSelect == 1)
							prices[6] = 2500;
						else if (giSelect == 2)
							prices[6] = 2300;
						else
							prices[6] = 2100;
					}
					else {

						ShowWindow(GetDlgItem(ghDlg, ID_CBSMPSPOWER), SW_HIDE);
						gMaterialList.smps = SMPSSea[1];
						prices[6] = 2500;
					}
				}
				gMaterialList.smpsPower = SMPSPower[1]; // This is for sure
				break;
				
			case ID_CBSMPSPOWER:
				giSelect = SendDlgItemMessage(ghDlg, ID_CBSMPSPOWER, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

				if (giSelect != 0) {
					gMaterialList.smpsPower = SMPSPower[giSelect];
				}

				break;

			case ID_CBCABINETCOMPANY:
				giSelect = SendDlgItemMessage(ghDlg, ID_CBCABINETCOMPANY, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

				switch (giSelect) {
				case 0:
					ShowWindow(GetDlgItem(ghDlg, ID_CBCABINET), SW_HIDE);
					gMaterialList.cabinetCompany = NULL;
					gMaterialList.cabinet = NULL;
					prices[7] = 0;
					break;

				case 1:
					SendDlgItemMessage(hDlg, ID_CBCABINET, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
					addItemsToCombobox(cabinetAntec, ID_CBCABINET, sizeof(cabinetAntec) / SIZE_OF_POINTER);
					ShowWindow(GetDlgItem(ghDlg, ID_CBCABINET), SW_SHOW);
					ShowWindow(GetDlgItem(ghDlg, ID_CBCABINETCOLOR), SW_HIDE);
					gMaterialList.cabinetCompany = cabinetCompany[giSelect];
					gMaterialList.cabinet = cabinetAntec[1];
					prices[7] = 1500;
					break;

				case 2:
					SendDlgItemMessage(hDlg, ID_CBCABINET, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
					addItemsToCombobox(cabinetCircle, ID_CBCABINET, sizeof(cabinetCircle) / SIZE_OF_POINTER);
					ShowWindow(GetDlgItem(ghDlg, ID_CBCABINET), SW_SHOW);
					ShowWindow(GetDlgItem(ghDlg, ID_CBCABINETCOLOR), SW_HIDE);
					gMaterialList.cabinetCompany = cabinetCompany[giSelect];
					gMaterialList.cabinet = cabinetCircle[1];
					prices[7] = 1200;
					break;

				case 3:
					SendDlgItemMessage(hDlg, ID_CBCABINET, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
					addItemsToCombobox(cabinetCorsair, ID_CBCABINET, sizeof(cabinetCorsair) / SIZE_OF_POINTER);
					ShowWindow(GetDlgItem(ghDlg, ID_CBCABINET), SW_SHOW);
					ShowWindow(GetDlgItem(ghDlg, ID_CBCABINETCOLOR), SW_HIDE);
					gMaterialList.cabinetCompany = cabinetCompany[giSelect];
					gMaterialList.cabinet = cabinetCorsair[1];
					prices[7] = 1800;
					break;

				}
				break;

			case ID_CBCABINET:
				giSelect = SendDlgItemMessage(ghDlg, ID_CBCABINET, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

				if (giSelect != 0) {
					SendDlgItemMessage(hDlg, ID_CBCABINETCOLOR, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
					addItemsToCombobox(cabinetColor, ID_CBCABINETCOLOR, sizeof(cabinetColor) / SIZE_OF_POINTER);
					ShowWindow(GetDlgItem(ghDlg, ID_CBCABINETCOLOR), SW_SHOW);
					if (!wcscmp(gMaterialList.cabinetCompany, cabinetCompany[1])) { //Antec

						switch (giSelect) {
						case 0:
							gMaterialList.cabinet = cabinetAntec[1];
							prices[7] = 1500;
							break;

						case 1:
							gMaterialList.cabinet = cabinetAntec[giSelect];
							prices[7] = 1500;
							break;

						case 2:
							gMaterialList.cabinet = cabinetAntec[giSelect];
							prices[7] = 1400;
							break;

						case 3:
							gMaterialList.cabinet = cabinetAntec[giSelect];
							prices[7] = 1300;
							break;
						}
					}
					else if (!wcscmp(gMaterialList.cabinetCompany, cabinetCompany[2])) { //Antec

						switch (giSelect) {
						case 0:
							gMaterialList.cabinet = cabinetCircle[1];
							prices[7] = 1200;
							break;

						case 1:
							gMaterialList.cabinet = cabinetCircle[giSelect];
							prices[7] = 1200;
							break;

						case 2:
							gMaterialList.cabinet = cabinetCircle[giSelect];
							prices[7] = 1100;
							break;

						case 3:
							gMaterialList.cabinet = cabinetCircle[giSelect];
							prices[7] = 1000;
							break;
						}
					}
					else if (!wcscmp(gMaterialList.cabinetCompany, cabinetCompany[3])) { //Antec

						switch (giSelect) {
						case 0:
							gMaterialList.cabinet = cabinetCorsair[1];
							prices[7] = 1800;
							break;

						case 1:
							gMaterialList.cabinet = cabinetCorsair[giSelect];
							prices[7] = 1800;
							break;

						case 2:
							gMaterialList.cabinet = cabinetCorsair[giSelect];
							prices[7] = 1700;
							break;

						case 3:
							gMaterialList.cabinet = cabinetCorsair[giSelect];
							prices[7] = 1600;
							break;
						}
					}
					gMaterialList.cabinetColor = cabinetColor[1];
				}
				
				break;


			case ID_CBCABINETCOLOR:
				giSelect = SendDlgItemMessage(ghDlg, ID_CBCABINETCOLOR, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

				if (giSelect != 0) {
					gMaterialList.cabinetColor = cabinetColor[giSelect];
				}
				break;

			case ID_CBKEYBOARDCOMPANY:
				giSelect = SendDlgItemMessage(ghDlg, ID_CBKEYBOARDCOMPANY, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

				if (giSelect != 0) {
					
					SendDlgItemMessage(hDlg, ID_CBKEYBOARD, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
					addItemsToCombobox(keyboardType, ID_CBKEYBOARD, sizeof(keyboardType) / SIZE_OF_POINTER);
					ShowWindow(GetDlgItem(ghDlg, ID_CBKEYBOARD), SW_SHOW);
					gMaterialList.keyboardCompany = keyboardCompany[giSelect];
					gMaterialList.keyboardType = keyboardType[1];
					prices[8] = 450;
				}
				else {
					ShowWindow(GetDlgItem(ghDlg, ID_CBKEYBOARD), SW_HIDE);
					gMaterialList.keyboardCompany = NULL;
					gMaterialList.keyboardType = NULL;
					prices[8] = 0;

				}

				break;

			case ID_CBKEYBOARD:

				giSelect = SendDlgItemMessage(ghDlg, ID_CBKEYBOARD, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

				if (!wcscmp(gMaterialList.keyboardCompany, keyboardCompany[1])) { //Microsoft
					if (giSelect == 1) {
						gMaterialList.keyboardType = keyboardType[giSelect];
						prices[8] = 450;
					}
					else if (giSelect == 2) {
						gMaterialList.keyboardType = keyboardType[giSelect];
						prices[8] = 650;
					}

				}
				else if (!wcscmp(gMaterialList.keyboardCompany, keyboardCompany[2])) {
					if (giSelect == 1) {
						gMaterialList.keyboardType = keyboardType[giSelect];
						prices[8] = 500;
					}
					else if (giSelect == 2) {
						gMaterialList.keyboardType = keyboardType[giSelect];
						prices[8] = 740;
					}

				}
				else if (!wcscmp(gMaterialList.keyboardCompany, keyboardCompany[3])) {
					if (giSelect == 1) {
						gMaterialList.keyboardType = keyboardType[giSelect];
						prices[8] = 490;
					}
					else if (giSelect == 2) {
						gMaterialList.keyboardType = keyboardType[giSelect];
						prices[8] = 670;
					}

				}
				break;

			case ID_CBMOUSECOMPANY:
				giSelect = SendDlgItemMessage(ghDlg, ID_CBMOUSECOMPANY, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

				if (giSelect != 0) {

					SendDlgItemMessage(hDlg, ID_CBMOUSETYPE, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
					addItemsToCombobox(mouseType, ID_CBMOUSETYPE, sizeof(mouseType) / SIZE_OF_POINTER);
					ShowWindow(GetDlgItem(ghDlg, ID_CBMOUSETYPE), SW_SHOW);
					gMaterialList.mouseCompany = mouseCompany[giSelect];
					gMaterialList.mouseType = mouseCompany[1];
					prices[9] = 370;
				}
				else {
					ShowWindow(GetDlgItem(ghDlg, ID_CBMOUSETYPE), SW_HIDE);
					gMaterialList.mouseCompany = NULL;
					gMaterialList.mouseType = NULL;
					prices[9] = 0;

				}

				break;

			case ID_CBMOUSETYPE:
				giSelect = SendDlgItemMessage(ghDlg, ID_CBMOUSETYPE, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

				if (!wcscmp(gMaterialList.mouseCompany, mouseCompany[1])) { //Microsoft
					if (giSelect == 1) {
						gMaterialList.mouseType = mouseCompany[giSelect];
						prices[9] = 370;
					}
					else if (giSelect == 2) {
						gMaterialList.mouseType = mouseType[giSelect];
						prices[9] = 650;
					}

				}
				else if (!wcscmp(gMaterialList.mouseCompany, mouseCompany[2])) {
					if (giSelect == 1) {
						gMaterialList.mouseType = mouseType[giSelect];
						prices[9] = 400;
					}
					else if (giSelect == 2) {
						gMaterialList.mouseType = mouseType[giSelect];
						prices[9] = 720;
					}

				}
				else if (!wcscmp(gMaterialList.mouseCompany, mouseCompany[3])) {
					if (giSelect == 1) {
						gMaterialList.mouseType = mouseType[giSelect];
						prices[9] = 350;
					}
					else if (giSelect == 2) {
						gMaterialList.mouseType = mouseType[giSelect];
						prices[9] = 700;
					}

				}
				break;


			case ID_CBMONITORCOMPANY:
				giSelect = SendDlgItemMessage(ghDlg, ID_CBMONITORCOMPANY, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

				switch (giSelect) {
				case 0:
					ShowWindow(GetDlgItem(ghDlg, ID_CBMONITORSERIES), SW_HIDE);
					ShowWindow(GetDlgItem(ghDlg, ID_CBMONITORSIZE), SW_HIDE);
					gMaterialList.monitorCompany = NULL;
					gMaterialList.monitor = NULL;
					gMaterialList.monitorSize = NULL;
					prices[10] = 0;
					break;

				case 1:
					SendDlgItemMessage(hDlg, ID_CBMONITORSERIES, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
					addItemsToCombobox(monitorBenQ, ID_CBMONITORSERIES, sizeof(monitorBenQ) / SIZE_OF_POINTER);
					ShowWindow(GetDlgItem(ghDlg, ID_CBMONITORSERIES), SW_SHOW);
					ShowWindow(GetDlgItem(ghDlg, ID_CBMONITORSIZE), SW_HIDE);
					gMaterialList.monitorCompany = monitorCompany[giSelect];
					gMaterialList.monitor = monitorBenQ[1];
					gMaterialList.monitorSize = monitorSize[1];
					prices[10] = 12300;
					break;

				case 2:
					SendDlgItemMessage(hDlg, ID_CBMONITORSERIES, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
					addItemsToCombobox(monitorAOC, ID_CBMONITORSERIES, sizeof(monitorAOC) / SIZE_OF_POINTER);
					ShowWindow(GetDlgItem(ghDlg, ID_CBMONITORSERIES), SW_SHOW);
					ShowWindow(GetDlgItem(ghDlg, ID_CBMONITORSIZE), SW_HIDE);
					gMaterialList.monitorCompany = monitorCompany[giSelect];
					gMaterialList.monitor = monitorAOC[1];
					gMaterialList.monitorSize = monitorSize[1];
					prices[10] = 11500;
					break;

				case 3:
					SendDlgItemMessage(hDlg, ID_CBMONITORSERIES, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
					addItemsToCombobox(monitorHP, ID_CBMONITORSERIES, sizeof(monitorHP) / SIZE_OF_POINTER);
					ShowWindow(GetDlgItem(ghDlg, ID_CBMONITORSERIES), SW_SHOW);
					ShowWindow(GetDlgItem(ghDlg, ID_CBMONITORSIZE), SW_HIDE);
					gMaterialList.monitorCompany = monitorCompany[giSelect];
					gMaterialList.monitor = monitorHP[1];
					gMaterialList.monitorSize = monitorSize[1];
					prices[10] = 10500;
					break;

				case 4:
					SendDlgItemMessage(hDlg, ID_CBMONITORSERIES, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
					addItemsToCombobox(monitorLG, ID_CBMONITORSERIES, sizeof(monitorLG) / SIZE_OF_POINTER);
					ShowWindow(GetDlgItem(ghDlg, ID_CBMONITORSERIES), SW_SHOW);
					ShowWindow(GetDlgItem(ghDlg, ID_CBMONITORSIZE), SW_HIDE);
					gMaterialList.monitorCompany = monitorCompany[giSelect];
					gMaterialList.monitor = monitorLG[1];
					gMaterialList.monitorSize = monitorSize[1];
					prices[10] = 14200;
					break;

				}
				break;

			case ID_CBMONITORSERIES:
				giSelect = SendDlgItemMessage(ghDlg, ID_CBMONITORSERIES, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

					if (!wcscmp(gMaterialList.monitorCompany, monitorCompany[1])) {

					if (giSelect != 0) {
						SendDlgItemMessage(hDlg, ID_CBMONITORSIZE, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
						addItemsToCombobox(monitorSize, ID_CBMONITORSIZE, sizeof(monitorSize) / SIZE_OF_POINTER);
						ShowWindow(GetDlgItem(ghDlg, ID_CBMONITORSIZE), SW_SHOW);
						gMaterialList.monitor = monitorBenQ[giSelect];
						if (giSelect == 1)
							prices[10] = 12300;
						else if (giSelect == 2)
							prices[10] = 11500;
						else
							prices[10] = 10900;
					}
					else {

						ShowWindow(GetDlgItem(ghDlg, ID_CBMONITORSIZE), SW_HIDE);
						gMaterialList.monitor = monitorBenQ[1];
						prices[10] = 12300;
					}
					gMaterialList.monitorSize = monitorSize[1]; // This is for sure
				}
				else if (!wcscmp(gMaterialList.monitorCompany, monitorCompany[2]))
				{
					if (giSelect != 0) {
						SendDlgItemMessage(hDlg, ID_CBMONITORSIZE, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
						addItemsToCombobox(monitorSize, ID_CBMONITORSIZE, sizeof(monitorSize) / SIZE_OF_POINTER);
						ShowWindow(GetDlgItem(ghDlg, ID_CBMONITORSIZE), SW_SHOW);
						gMaterialList.monitor = monitorAOC[giSelect];
						if (giSelect == 1)
							prices[10] = 11500;
						else if (giSelect == 2)
							prices[10] = 11000;

					}
					else {

						ShowWindow(GetDlgItem(ghDlg, ID_CBMONITORSIZE), SW_HIDE);
						gMaterialList.monitor = monitorAOC[1];
						prices[10] = 11500;
					}
					gMaterialList.monitorSize = monitorSize[1]; // This is for sure
				}
				else if (!wcscmp(gMaterialList.monitorCompany, monitorCompany[3]))
				{
					if (giSelect != 0) {
						SendDlgItemMessage(hDlg, ID_CBMONITORSIZE, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
						addItemsToCombobox(monitorSize, ID_CBMONITORSIZE, sizeof(monitorSize) / SIZE_OF_POINTER);
						ShowWindow(GetDlgItem(ghDlg, ID_CBMONITORSIZE), SW_SHOW);
						gMaterialList.monitor = monitorHP[giSelect];
						if (giSelect == 1)
							prices[10] = 10500;
						else if (giSelect == 2)
							prices[10] = 10000;

					}
					else {

						ShowWindow(GetDlgItem(ghDlg, ID_CBMONITORSIZE), SW_HIDE);
						gMaterialList.monitor = monitorHP[1];
						prices[10] = 10500;
					}
					gMaterialList.monitorSize = monitorSize[1]; // This is for sure
				}
				else if (!wcscmp(gMaterialList.monitorCompany, monitorCompany[4]))
				{
					if (giSelect != 0) {
						SendDlgItemMessage(hDlg, ID_CBMONITORSIZE, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
						addItemsToCombobox(monitorSize, ID_CBMONITORSIZE, sizeof(monitorSize) / SIZE_OF_POINTER);
						ShowWindow(GetDlgItem(ghDlg, ID_CBMONITORSIZE), SW_SHOW);
						gMaterialList.monitor = monitorLG[giSelect];
						if (giSelect == 1)
							prices[10] = 14500;
						else if (giSelect == 2)
							prices[10] = 13000;

					}
					else {

						ShowWindow(GetDlgItem(ghDlg, ID_CBMONITORSIZE), SW_HIDE);
						gMaterialList.monitor = monitorLG[1];
						prices[10] = 14500;
					}
					gMaterialList.monitorSize = monitorSize[1]; // This is for sure
				}
				
				break;

			case ID_CBMONITORSIZE:
				giSelect = SendDlgItemMessage(ghDlg, ID_CBMONITORSIZE, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

				if (giSelect != 0) {
					gMaterialList.monitorSize = monitorSize[giSelect];
				}

				break;



			case ID_CBPRINTERCOMPANY:
				giSelect = SendDlgItemMessage(ghDlg, ID_CBPRINTERCOMPANY, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

				switch (giSelect) {
				case 0:
					ShowWindow(GetDlgItem(ghDlg, ID_CBPRINTERTYPE), SW_HIDE);
					ShowWindow(GetDlgItem(ghDlg, ID_CBPRINTERCONNECT), SW_HIDE);
					gMaterialList.printerCompany = NULL;
					gMaterialList.printer = NULL;
					gMaterialList.printerType = NULL;
					prices[11] = 0;
					break;

				case 1:
					SendDlgItemMessage(hDlg, ID_CBPRINTERTYPE, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
					addItemsToCombobox(printerHP, ID_CBPRINTERTYPE, sizeof(printerHP) / SIZE_OF_POINTER);
					ShowWindow(GetDlgItem(ghDlg, ID_CBPRINTERTYPE), SW_SHOW);
					ShowWindow(GetDlgItem(ghDlg, ID_CBPRINTERCONNECT), SW_HIDE);
					gMaterialList.printerCompany = printerCompany[giSelect];
					gMaterialList.printer = printerHP[1];
					gMaterialList.printerType = printerConnect[1];
					prices[11] = 20300;
					break;

				case 2:
					SendDlgItemMessage(hDlg, ID_CBPRINTERTYPE, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
					addItemsToCombobox(printerEpson, ID_CBPRINTERTYPE, sizeof(printerEpson) / SIZE_OF_POINTER);
					ShowWindow(GetDlgItem(ghDlg, ID_CBPRINTERTYPE), SW_SHOW);
					ShowWindow(GetDlgItem(ghDlg, ID_CBPRINTERCONNECT), SW_HIDE);
					gMaterialList.printerCompany = printerCompany[giSelect];
					gMaterialList.printer = printerEpson[1];
					gMaterialList.printerType = printerConnect[1];
					prices[11] = 25300;
					break;

				case 3:
					SendDlgItemMessage(hDlg, ID_CBPRINTERTYPE, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
					addItemsToCombobox(printerCanon, ID_CBPRINTERTYPE, sizeof(printerCanon) / SIZE_OF_POINTER);
					ShowWindow(GetDlgItem(ghDlg, ID_CBPRINTERTYPE), SW_SHOW);
					ShowWindow(GetDlgItem(ghDlg, ID_CBPRINTERCONNECT), SW_HIDE);
					gMaterialList.printerCompany = printerCompany[giSelect];
					gMaterialList.printer = printerCanon[1];
					gMaterialList.printerType = printerConnect[1];
					prices[11] = 24500;
					break;

				}
				break;

			case ID_CBPRINTERTYPE:
				giSelect = SendDlgItemMessage(ghDlg, ID_CBPRINTERTYPE, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

				if (!wcscmp(gMaterialList.printerCompany, printerCompany[1])) {

					if (giSelect != 0) {
						SendDlgItemMessage(hDlg, ID_CBPRINTERCONNECT, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
						addItemsToCombobox(printerConnect, ID_CBPRINTERCONNECT, sizeof(printerConnect) / SIZE_OF_POINTER);
						ShowWindow(GetDlgItem(ghDlg, ID_CBPRINTERCONNECT), SW_SHOW);
						gMaterialList.printer = printerHP[giSelect];
						if (giSelect == 1)
							prices[11] = 20300;
						else if (giSelect == 2)
							prices[11] = 19700;
						else
							prices[11] = 22000;
					}
					else {

						ShowWindow(GetDlgItem(ghDlg, ID_CBPRINTERCONNECT), SW_HIDE);
						gMaterialList.printer = printerHP[1];
						prices[11] = 20300;
					}

				}
				else if (!wcscmp(gMaterialList.printerCompany, printerCompany[2])) {

					if (giSelect != 0) {
						SendDlgItemMessage(hDlg, ID_CBPRINTERCONNECT, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
						addItemsToCombobox(printerConnect, ID_CBPRINTERCONNECT, sizeof(printerConnect) / SIZE_OF_POINTER);
						ShowWindow(GetDlgItem(ghDlg, ID_CBPRINTERCONNECT), SW_SHOW);
						gMaterialList.printer = printerEpson[giSelect];
						if (giSelect == 1)
							prices[11] = 25300;
						else if (giSelect == 2)
							prices[11] = 22400;

					}
					else {

						ShowWindow(GetDlgItem(ghDlg, ID_CBPRINTERCONNECT), SW_HIDE);
						gMaterialList.printer = printerEpson[1];
						prices[11] = 25300;
					}

				}
				else if (!wcscmp(gMaterialList.printerCompany, printerCompany[3])) {

					if (giSelect != 0) {
						SendDlgItemMessage(hDlg, ID_CBPRINTERCONNECT, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
						addItemsToCombobox(printerConnect, ID_CBPRINTERCONNECT, sizeof(printerConnect) / SIZE_OF_POINTER);
						ShowWindow(GetDlgItem(ghDlg, ID_CBPRINTERCONNECT), SW_SHOW);
						gMaterialList.printer = printerCanon[giSelect];
						if (giSelect == 1)
							prices[11] = 24500;
						else if (giSelect == 2)
							prices[11] = 21700;
						else
							prices[11] = 22400;
					}
					else {

						ShowWindow(GetDlgItem(ghDlg, ID_CBPRINTERCONNECT), SW_HIDE);
						gMaterialList.printer = printerCanon[1];
						prices[11] = 24500;
					}

				}
				
				gMaterialList.monitorSize = monitorSize[1]; // This is for sure
				break;

			case ID_CBPRINTERCONNECT:
				giSelect = SendDlgItemMessage(ghDlg, ID_CBPRINTERCONNECT, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

				if (giSelect != 0) {
					gMaterialList.printerType = printerConnect[giSelect];
				}

				break;

			default:
				MessageBox(ghDlg, TEXT("YOU ARE IN CBN_SELCHANGE DEFAULT"), TEXT("ERROR"), MB_OK);
				break;
			}

			break;
		}
		return TRUE;

	}

	return FALSE;

}


BOOL CALLBACK MyReceiptDialogProc(HWND hDlg, UINT iMsg, WPARAM wParam, LPARAM lParam) {

	void writeReceiptFile(TCHAR *);
	
	switch (iMsg)
	{
	case WM_INITDIALOG:
		SetFocus(GetDlgItem(hDlg, ID_ETNAME));
		
		return(TRUE);


	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case ID_PBCONTINUE:
			EndDialog(hDlg, 0);
			break;

		case IDOK:
			GetDlgItemText(hDlg, ID_ETNAME, name, 50);
			writeReceiptFile(name);
			receiptSavedFlag = true;
			DialogBox(hInst, TEXT("FILEIO"), gHwnd, MyFileIODialogProc);
			EndDialog(hDlg, 0);
			break;

		case IDCANCEL:
			EndDialog(hDlg, 0);
			break;
		}

		return TRUE;

	}

	return FALSE;
}


BOOL CALLBACK MyFileIODialogProc(HWND hDlg, UINT iMsg, WPARAM wParam, LPARAM lParam) {

	void writeReceiptFile(TCHAR *);
	switch (iMsg)
	{
	case WM_INITDIALOG:
		return(TRUE);


	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDOK:
			InvalidateRect(gHwnd, &rc, true);
			EndDialog(hDlg, 0);
			break;

		case IDCANCEL:
			EndDialog(hDlg, 0);
			break;
		}

		return TRUE;

	}

	return FALSE;
}


bool paintMainWindow(void) {

	void refillTempArray(void);
	void formatStringForFile(TCHAR *buffer, TCHAR *data1, TCHAR *data2, TCHAR *data3, TCHAR *data4, TCHAR *data5, TCHAR *data6, TCHAR *data7);

	HDC hdcInst, hdcBitmap;
	PAINTSTRUCT ps;
	BITMAP bp;
	RECT r;
	HBRUSH hBrush;
	HPEN hPen;
	SIZE sz;
	int paraMargin = 0, resetMargin;
	int c;
	
	hdcInst = BeginPaint(gHwnd, &ps);
	if (splashScreenActive) {


		hInst = (HINSTANCE)GetWindowLong(gHwnd, GWLP_HINSTANCE);
		hBitmap = LoadBitmap(hInst, MAKEINTRESOURCE(IDB_SPLASHID));
		// Create a memory device compatible with the above DC variable
		hdcBitmap = CreateCompatibleDC(hdcInst);
		// Select the new bitmap
		SelectObject(hdcBitmap, hBitmap);
		GetObject(hBitmap, sizeof(bp), &bp);
		// Get client coordinates for the StretchBlt() function
		GetClientRect(gHwnd, &r);
		// stretch bitmap across client area
		StretchBlt(hdcInst, 0, 0, r.right - r.left, r.bottom - r.top, hdcBitmap, 0,
			0, bp.bmWidth, bp.bmHeight, SRCCOPY);

		//Title on material page
		SelectObject(hdcInst, CreateFont(40, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, ANSI_CHARSET,
			OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE,
			TEXT("Broadway")));
		SetTextColor(hdcInst, RGB(150, 0, 0));
		SetBkColor(hdcInst, TRANSPARENT);
		SetBkMode(hdcInst, TRANSPARENT);
		TextOut(hdcInst, 100, 540, TEXT("Welcome to Vivid Computer Shop...."), 32);

		SelectObject(hdcInst, CreateFont(30, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, ANSI_CHARSET,
			OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE,
			TEXT("Times New Roman")));
		SetTextColor(hdcInst, RGB(255, 255, 255));
		SetBkColor(hdcInst, TRANSPARENT);
		SetBkMode(hdcInst, TRANSPARENT);
		TextOut(hdcInst, 100, 590, TEXT("Hit Spacebar to continue...."), 28);
		

		// Cleanup
		DeleteDC(hdcBitmap);

	}
	else
	{
		GetClientRect(gHwnd, &rc);
		hBrush = CreateSolidBrush((COLORREF)RGB(0, 0, 0));
		FillRect(hdcInst, &ps.rcPaint, hBrush);
		//hPen = CreatePen(PS_SOLID, 3, (COLORREF)RGB(255, 255, 0));
		hPen = CreatePen(PS_SOLID, 3, (COLORREF)RGB(100, 100, 100));
		SelectObject(hdcInst, hPen);
		//Title section
		MoveToEx(hdcInst, 0, ps.rcPaint.top + 0x50, NULL);
		LineTo(hdcInst, ps.rcPaint.right, ps.rcPaint.top + 0x50);
		MoveToEx(hdcInst, 0, ps.rcPaint.top + 0x75, NULL);
		LineTo(hdcInst, ps.rcPaint.right, ps.rcPaint.top + 0x75);
		//total section
		MoveToEx(hdcInst, 0, ps.rcPaint.top + 0x1cf, NULL);
		LineTo(hdcInst, ps.rcPaint.right, ps.rcPaint.top + 0x1cf);
		MoveToEx(hdcInst, 0, ps.rcPaint.top + 0x1ef, NULL);
		LineTo(hdcInst, ps.rcPaint.right, ps.rcPaint.top + 0x1ef);
		//cloumns
		MoveToEx(hdcInst, ps.rcPaint.top + 0xa0, ps.rcPaint.top + 0x50, NULL);
		LineTo(hdcInst, ps.rcPaint.top + 0xa0, ps.rcPaint.top + 0x1cf);
		MoveToEx(hdcInst, ps.rcPaint.top + 0x165, ps.rcPaint.top + 0x50, NULL);
		LineTo(hdcInst, ps.rcPaint.top + 0x165, ps.rcPaint.top + 0x1cf);
		MoveToEx(hdcInst, ps.rcPaint.top + 0x210, ps.rcPaint.top + 0x50, NULL);
		LineTo(hdcInst, ps.rcPaint.top + 0x210, ps.rcPaint.top + 0x1cf);
		MoveToEx(hdcInst, ps.rcPaint.top + 0x295, ps.rcPaint.top + 0x50, NULL);
		LineTo(hdcInst, ps.rcPaint.top + 0x295, ps.rcPaint.top + 0x1cf);
		MoveToEx(hdcInst, ps.rcPaint.top + 0x345, ps.rcPaint.top + 0x50, NULL);
		LineTo(hdcInst, ps.rcPaint.top + 0x345, ps.rcPaint.top + 0x1cf);
		MoveToEx(hdcInst, ps.rcPaint.top + 0x390, ps.rcPaint.top + 0x50, NULL);
		LineTo(hdcInst, ps.rcPaint.top + 0x390, ps.rcPaint.top + 0x1ef);
		MoveToEx(hdcInst, ps.rcPaint.top + 0x470, ps.rcPaint.top + 0x50, NULL);
		LineTo(hdcInst, ps.rcPaint.top + 0x470, ps.rcPaint.top + 0x1cf);
		
		SelectObject(hdcInst, CreateFont(20, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, ANSI_CHARSET,
			OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE,
			TEXT("Cambria Bold")));
		SetTextColor(hdcInst, RGB(0, 255, 100));
		SetBkColor(hdcInst, TRANSPARENT);
		SetBkMode(hdcInst, TRANSPARENT);

		// Add column headings		
		TextOut(hdcInst, ps.rcPaint.top + 0x2A, ps.rcPaint.top + 0x5b, headings[0], wcslen(headings[0]));
		TextOut(hdcInst, ps.rcPaint.top + 0xCF, ps.rcPaint.top + 0x5b, headings[1], wcslen(headings[1]));
		TextOut(hdcInst, ps.rcPaint.top + 0x185, ps.rcPaint.top + 0x5b, headings[2], wcslen(headings[2]));
		TextOut(hdcInst, ps.rcPaint.top + 0x21C, ps.rcPaint.top + 0x5b, headings[3], wcslen(headings[3]));
		TextOut(hdcInst, ps.rcPaint.top + 0x2C5, ps.rcPaint.top + 0x5b, headings[4], wcslen(headings[4]));
		TextOut(hdcInst, ps.rcPaint.top + 0x355, ps.rcPaint.top + 0x5b, headings[5], wcslen(headings[5]));
		TextOut(hdcInst, ps.rcPaint.top + 0x3E0, ps.rcPaint.top + 0x5b, headings[6], wcslen(headings[6]));
		TextOut(hdcInst, ps.rcPaint.top + 0x320, ps.rcPaint.top + 0x1d7, headings[6], wcslen(headings[6]));

		formatStringForFile(fileIOSrings.headingR, headings[0], headings[1], headings[2], headings[3], headings[4],
			headings[5], headings[6]);
	
		SelectObject(hdcInst, CreateFont(20, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, ANSI_CHARSET,
			OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE,
			TEXT("Arial Bold")));
		SetTextColor(hdcInst, RGB(255, 0, 127));
		GetTextExtentPoint32(hdcInst, headings[0], wcslen(headings[0]), &sz);

		for (c = 0; c < 12; c++) {

			TextOut(hdcInst, ps.rcPaint.top + 0x5, ps.rcPaint.top + 0x85 +paraMargin, items[c], wcslen(items[c]));
			paraMargin += sz.cy + 7;
		}
		
		paraMargin = ps.rcPaint.top + 0x85;
		resetMargin = paraMargin;
		//fill device column
		temp[0] = gMaterialList.cpuCompany;
		temp[1] = gMaterialList.ramCompany;
		temp[2] = gMaterialList.motherBoardCompany;
		temp[3] = gMaterialList.gcCompany;
		temp[4] = gMaterialList.hdCompany;
		temp[5] = gMaterialList.cdDvdDriveCompany;
		temp[6] = gMaterialList.smpsCompany;
		temp[7] = gMaterialList.cabinetCompany;
		temp[8] = gMaterialList.keyboardCompany;
		temp[9] = gMaterialList.mouseCompany;
		temp[10] = gMaterialList.monitorCompany;
		temp[11] = gMaterialList.printerCompany;
		
		for (c = 0; c < 12; c++) {

			if (temp[c] != NULL)
				TextOut(hdcInst, 0xaa, paraMargin, temp[c], wcslen(temp[c]));
			else
				TextOut(hdcInst, 0xaa, paraMargin, TEXT("-"), 1);

			paraMargin += sz.cy + 7;
		}
		//refill temp array for subtype column
		refillTempArray();
		paraMargin = resetMargin;
		if (gMaterialList.cpuProcessor != NULL) {
			temp[0] = gMaterialList.cpuProcessor;
			temp[2] = gMaterialList.motherBoard;
			temp[3] = gMaterialList.gcSerial;
			temp[5] = gMaterialList.cdDvdDrive;
			temp[6] = gMaterialList.smps;
			temp[7] = gMaterialList.cabinet;
			temp[10] = gMaterialList.monitor;
			//temp[11] = gMaterialList.printerType;
 		}
			
		for (c = 0; c < 12; c++) {
			if (temp[c] == NULL)
			{
				temp[c] = TEXT("-");
 			}
				TextOut(hdcInst, 0x170, paraMargin, temp[c], wcslen(temp[c]));
				paraMargin += sz.cy + 7;
		}
		//refill temp array for generation column
		refillTempArray();
		paraMargin = resetMargin;
		if (gMaterialList.cpuGeneration != NULL)
			temp[0] = gMaterialList.cpuGeneration;
		for (c = 0; c < 12; c++) {
			if (temp[c] == NULL)
			{
				temp[c] = TEXT("-");
			}
			TextOut(hdcInst, 0x21a, paraMargin, temp[c], wcslen(temp[c]));
			paraMargin += sz.cy + 7;
		}
		//refill temp array for model column
		refillTempArray();
		paraMargin = resetMargin;
		if (gMaterialList.cpuModel != NULL)
		{
			temp[0] = gMaterialList.cpuModel;
			temp[6] = gMaterialList.smpsPower;
			temp[7] = gMaterialList.cabinetColor;
			temp[8] = gMaterialList.keyboardType;
			temp[9] = gMaterialList.mouseType;
			temp[11] = gMaterialList.printerType;
		}
		
		for (c = 0; c < 12; c++) {
			if (temp[c] == NULL)
			{
				temp[c] = TEXT("-");
			}
			TextOut(hdcInst, 0x29f, paraMargin, temp[c], wcslen(temp[c]));
			paraMargin += sz.cy + 7;
		}
		//refill temp array for SIZE column
		refillTempArray();
		paraMargin = resetMargin;
		if (gMaterialList.ramSize != NULL)
			temp[1] = gMaterialList.ramSize;
		if (gMaterialList.gcSize != NULL)
			temp[3] = gMaterialList.gcSize;
		if (gMaterialList.hdSize != NULL)
			temp[4] = gMaterialList.hdSize;
		if (gMaterialList.monitorSize != NULL)
			temp[10] = gMaterialList.monitorSize;
		for (c = 0; c < 12; c++) {

			TextOut(hdcInst, 0x34f, paraMargin, temp[c], wcslen(temp[c]));
			paraMargin += sz.cy + 7;
		}
		//refill temp array for SIZE column
		refillTempArray();
		paraMargin = resetMargin;

		for (c = 0; c < 12; c++) {

			if (temp[c] == NULL)
			{
				temp[c] = TEXT("-");
			}

			if(prices[c] == 0)
			TextOut(hdcInst, 0x39a, paraMargin, TEXT("-"), 1);
			else
			{
				TextOut(hdcInst, 0x39a, paraMargin, _itow(prices[c], tempPrice, 10), wcslen(_itow(prices[c], tempPrice, 10)));
			}
			paraMargin += sz.cy + 7;
		}
		// price
		for (c = 0; c < 12; c++) {

			totalPrice += prices[c];
		}
		TextOut(hdcInst, 0x39a, paraMargin + sz.cy + 7, TEXT("RS."), 3);
		TextOut(hdcInst, 0x3C0, paraMargin + sz.cy + 7, _itow(totalPrice, tempPrice, 10), wcslen(_itow(totalPrice, tempPrice, 10)));
		//top
		GetClientRect(gHwnd, &rc);
		SelectObject(hdcInst, CreateFont(30, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, ANSI_CHARSET,
			OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE,
			TEXT("Broadway")));
		SetTextColor(hdcInst, RGB(204, 204, 204));
		DrawText(hdcInst, TEXT("------Vivid Computer Shop------"), -1, &rc, DT_SINGLELINE | DT_CENTER);
		//bottom
		SelectObject(hdcInst, CreateFont(25, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, ANSI_CHARSET,
			OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE,
			TEXT("Times New Roman Bold")));
		SetTextColor(hdcInst, RGB(204, 102, 0));
		if(receiptSavedFlag) 
		TextOut(hdcInst, 0x40, 0x220, TEXT("**THANK YOU for shopping with us. Press 'ESC' to exit."), 55);
		else
		TextOut(hdcInst, 0x40, 0x220, TEXT("**Press 'P' to get the recipt or 'ESC' to end shopping.."), 55);
	}
	EndPaint(gHwnd, &ps);
	 
	return 0;

}


bool initializeDialogBox(void) {

	void addItemsToCombobox(TCHAR **, int, int);
	
	//Hide sub-selections
	ShowWindow(GetDlgItem(ghDlg, ID_CBCPUPROCESSOR), SW_HIDE);
	ShowWindow(GetDlgItem(ghDlg, ID_CBCPUMODEL), SW_HIDE);
	ShowWindow(GetDlgItem(ghDlg, ID_CBCPUGENERATION), SW_HIDE);
	ShowWindow(GetDlgItem(ghDlg, ID_CBRAMSIZE), SW_HIDE);
	ShowWindow(GetDlgItem(ghDlg, ID_CBGCSIZE), SW_HIDE);
	ShowWindow(GetDlgItem(ghDlg, ID_CBGCSERIAL1), SW_HIDE);
	ShowWindow(GetDlgItem(ghDlg, ID_CBHDSIZE), SW_HIDE);
	ShowWindow(GetDlgItem(ghDlg, ID_CBSMPS), SW_HIDE);
	ShowWindow(GetDlgItem(ghDlg, ID_CBMONITORSERIES), SW_HIDE);

	ShowWindow(GetDlgItem(ghDlg, ID_CBMOTHERBOSRD), SW_HIDE); 
	ShowWindow(GetDlgItem(ghDlg, ID_CBCDDVDDRIVE), SW_HIDE);
	ShowWindow(GetDlgItem(ghDlg, ID_CBSMPSPOWER), SW_HIDE);
	ShowWindow(GetDlgItem(ghDlg, ID_CBCABINET), SW_HIDE);
	ShowWindow(GetDlgItem(ghDlg, ID_CBKEYBOARD), SW_HIDE);
	ShowWindow(GetDlgItem(ghDlg, ID_CBKEYBOARDCOLOR), SW_HIDE);
	ShowWindow(GetDlgItem(ghDlg, ID_CBMOUSETYPE), SW_HIDE);
	ShowWindow(GetDlgItem(ghDlg, ID_CBMONITORSIZE), SW_HIDE);
	ShowWindow(GetDlgItem(ghDlg, ID_CBPRINTERTYPE), SW_HIDE);
	ShowWindow(GetDlgItem(ghDlg, ID_CBPRINTERCONNECT), SW_HIDE);
	ShowWindow(GetDlgItem(ghDlg, ID_CBCABINETCOLOR), SW_HIDE);
	
	//add strings to all comboboxes
	addItemsToCombobox(cpuCompany,			ID_CBCPUCOMPANY,		sizeof(cpuCompany) / SIZE_OF_POINTER);//CPU
	addItemsToCombobox(RAMCompany,			ID_CBRAMCOMPANY,		sizeof(RAMCompany) / SIZE_OF_POINTER); //RAM
	addItemsToCombobox(motherBoardCompany,	ID_CBMOTHERBOSRDCOMPANY,sizeof(motherBoardCompany) / SIZE_OF_POINTER);//MotherBoard
	addItemsToCombobox(graphicsCardCompany,	ID_CBGCCOMPANY,			sizeof(graphicsCardCompany) / SIZE_OF_POINTER);//Graphics Card	
	addItemsToCombobox(hardDiskCompany,		ID_CBHDCOMPANY,			sizeof(hardDiskCompany) / SIZE_OF_POINTER);// Hard disk	
	addItemsToCombobox(CDDVDDriveCompany,	ID_CBCDDVDDRIVECOMPANY,	sizeof(CDDVDDriveCompany) / SIZE_OF_POINTER);//CD/DVD Drive
	addItemsToCombobox(SMPSCompany,			ID_CBSMPSCOMPANY,		sizeof(SMPSCompany) / SIZE_OF_POINTER);//SMPS	
	addItemsToCombobox(cabinetCompany,		ID_CBCABINETCOMPANY,	sizeof(cabinetCompany) / SIZE_OF_POINTER);//Cabinet	
	addItemsToCombobox(keyboardCompany,		ID_CBKEYBOARDCOMPANY,	sizeof(keyboardCompany) / SIZE_OF_POINTER);//Keyboard
	addItemsToCombobox(mouseCompany,		ID_CBMOUSECOMPANY,		sizeof(mouseCompany) / SIZE_OF_POINTER);//Mouse	
	addItemsToCombobox(monitorCompany,		ID_CBMONITORCOMPANY,	sizeof(monitorCompany) / SIZE_OF_POINTER);//Monitor
	addItemsToCombobox(printerCompany,		ID_CBPRINTERCOMPANY,	sizeof(printerCompany) / SIZE_OF_POINTER); // Printer
	return true;
}


/*this function takes pointer of char pointer array and combobox
ID as an argument and fills the combobox with the items*/
void addItemsToCombobox(TCHAR **src, int ID_CB, int sizeOfArray) {
	int j = 5;
	TCHAR *tempSelectOption;
	tempSelectOption = *src;
	for (j = 0; j < sizeOfArray; j++ ) //sizeof(src) / SIZE_OF_POINTER
	{
		SendDlgItemMessage(ghDlg, ID_CB, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCSTR)src[0]);
		*src++;
	}
	SendDlgItemMessage(ghDlg, ID_CB, CB_SELECTSTRING, (WPARAM)0, (LPARAM)(LPCSTR)tempSelectOption);
}


void refillTempArray(void) {
	int c;

	//wmemset((wchar_t *)temp, '-', 12);
	for (c = 0; c < 12; c++) {
	temp[c] = TEXT("-");
	}

}

void writeReceiptFile(TCHAR * fileName) {

	void formatStringForFile(TCHAR *buffer, TCHAR *data1, TCHAR *data2, TCHAR *data3, TCHAR *data4, TCHAR *data5, TCHAR *data6, TCHAR *data7); //, TCHAR *data8,
	void writeLineIntoFile(TCHAR data[]);

	TCHAR fName[60];
	ua_wcscpy(fName, fileName);	  
	fp = _wfopen(wcscat(fileName, TEXT("-Receipt.txt")), TEXT("w, ccs=UNICODE"));

	if (fp != NULL) {
		writeLineIntoFile(L"*******************************************************Vivid Computer Shop**********************************************************\n\n\n\n");
		gwInvoiceNumber++;
		wsprintf(str, TEXT("\r  Name : %s                                                                         Invoice No : %d \n\n"), fName, gwInvoiceNumber);
		writeLineIntoFile(L"--------------------------------------------------------------------------------------------------------------------------------------\n");
		writeLineIntoFile(str);
		writeLineIntoFile(L"--------------------------------------------------------------------------------------------------------------------------------------\n");
		writeLineIntoFile(fileIOSrings.headingR);
		writeLineIntoFile(L"\n\r--------------------------------------------------------------------------------------------------------------------------------------\n");
		//writeLineIntoFile(L"\n");

	if (prices[0] != 0) {
		
		formatStringForFile(fileIOSrings.cpuCompanyR,
			items[0],
			(gMaterialList.cpuCompany != NULL ? gMaterialList.cpuCompany : TEXT("-")),
			(gMaterialList.cpuProcessor != NULL ? gMaterialList.cpuProcessor : TEXT("-")),
			(gMaterialList.cpuGeneration != NULL ? gMaterialList.cpuGeneration : TEXT("-")),
			(gMaterialList.cpuModel != NULL ? gMaterialList.cpuModel : TEXT("-")),
			TEXT("-"),
			_itow(prices[0], tempPrice, 10));
		writeLineIntoFile(fileIOSrings.cpuCompanyR);
		writeLineIntoFile(L"\n");
	}

	if (prices[1] != 0) {

		formatStringForFile(fileIOSrings.ramR,
			items[1],
			(gMaterialList.ramCompany != NULL ? gMaterialList.ramCompany : TEXT("-")),
			TEXT("-"), 
			TEXT("-"),
			TEXT("-"),
			(gMaterialList.ramSize != NULL ? gMaterialList.ramSize : TEXT("-")),
			_itow(prices[1], tempPrice, 10));
		writeLineIntoFile(fileIOSrings.ramR);
		writeLineIntoFile(L"\n");

	}

	if (prices[2] != 0) {

		formatStringForFile(fileIOSrings.motherBoardR,
			items[2],
			(gMaterialList.motherBoardCompany != NULL ? gMaterialList.motherBoardCompany : TEXT("-")),
			(gMaterialList.motherBoard != NULL ? gMaterialList.motherBoard : TEXT("-")),
			TEXT("-"),
			TEXT("-"),
			TEXT("-"),
			_itow(prices[2], tempPrice, 10));
		writeLineIntoFile(fileIOSrings.motherBoardR);
		writeLineIntoFile(L"\n");

	}

	if (prices[3] != 0) {

		formatStringForFile(fileIOSrings.gcCompanyR,
			items[3],
			(gMaterialList.gcCompany != NULL ? gMaterialList.gcCompany : TEXT("-")),
			(gMaterialList.gcSerial != NULL ? gMaterialList.gcSerial : TEXT("-")),
			TEXT("-"),
			TEXT("-"),
			(gMaterialList.gcSize != NULL ? gMaterialList.gcSize : TEXT("-")),
			_itow(prices[3], tempPrice, 10));
		writeLineIntoFile(fileIOSrings.gcCompanyR);
		writeLineIntoFile(L"\n");

	}

	if (prices[4] != 0) {

		formatStringForFile(fileIOSrings.hdCompanyR,
			items[4],
			(gMaterialList.hdCompany != NULL ? gMaterialList.hdCompany : TEXT("-")),
			TEXT("-"),
			TEXT("-"),
			TEXT("-"),
			(gMaterialList.hdSize != NULL ? gMaterialList.hdSize : TEXT("-")),
			_itow(prices[4], tempPrice, 10));
		writeLineIntoFile(fileIOSrings.hdCompanyR);
		writeLineIntoFile(L"\n");

	}

	if (prices[5] != 0) {

		formatStringForFile(fileIOSrings.cdDvdDriveR,
			items[5],
			(gMaterialList.cdDvdDriveCompany != NULL ? gMaterialList.cdDvdDriveCompany : TEXT("-")),
			(gMaterialList.cdDvdDrive != NULL ? gMaterialList.cdDvdDrive : TEXT("-")),
			TEXT("-"),
			TEXT("-"),
			TEXT("-"),
			_itow(prices[5], tempPrice, 10));
		writeLineIntoFile(fileIOSrings.cdDvdDriveR);
		writeLineIntoFile(L"\n");

	}

	if (prices[6] != 0) {

		formatStringForFile(fileIOSrings.smpsR,
			items[6],
			(gMaterialList.smpsCompany != NULL ? gMaterialList.smpsCompany : TEXT("-")),
			(gMaterialList.smps != NULL ? gMaterialList.smps : TEXT("-")),
			TEXT("-"),
			(gMaterialList.smpsPower != NULL ? gMaterialList.smpsPower : TEXT("-")),
			TEXT("-"),
			_itow(prices[6], tempPrice, 10));
		writeLineIntoFile(fileIOSrings.smpsR);
		writeLineIntoFile(L"\n");
	}

	if (prices[7] != 0) {

		formatStringForFile(fileIOSrings.cabinetR,
			items[7],
			(gMaterialList.cabinetCompany != NULL ? gMaterialList.cabinetCompany : TEXT("-")),
			(gMaterialList.cabinet != NULL ? gMaterialList.cabinet : TEXT("-")),
			TEXT("-"),
			(gMaterialList.cabinetColor != NULL ? gMaterialList.cabinetColor : TEXT("-")),
			TEXT("-"),
			_itow(prices[7], tempPrice, 10));
		writeLineIntoFile(fileIOSrings.cabinetR);
		writeLineIntoFile(L"\n");
	}

	if (prices[8] != 0) {

		formatStringForFile(fileIOSrings.keyboardR,
			items[8],
			(gMaterialList.keyboardCompany != NULL ? gMaterialList.keyboardCompany : TEXT("-")),
			(gMaterialList.keyboardType != NULL ? gMaterialList.keyboardType : TEXT("-")),
			TEXT("-"),
			TEXT("-"),
			TEXT("-"),
			_itow(prices[6], tempPrice, 10));
		writeLineIntoFile(fileIOSrings.keyboardR);
		writeLineIntoFile(L"\n");
	}

	if (prices[9] != 0) {

		formatStringForFile(fileIOSrings.mouseR,
			items[9],
			(gMaterialList.mouseCompany != NULL ? gMaterialList.mouseCompany : TEXT("-")),
			(gMaterialList.mouseType != NULL ? gMaterialList.mouseType : TEXT("-")),
			TEXT("-"),
			TEXT("-"),
			TEXT("-"),
			_itow(prices[9], tempPrice, 10));
		writeLineIntoFile(fileIOSrings.mouseR);
		writeLineIntoFile(L"\n");
	}

	if (prices[10] != 0) {

		formatStringForFile(fileIOSrings.monitorR,
			items[10],
			(gMaterialList.monitorCompany != NULL ? gMaterialList.monitorCompany : TEXT("-")),
			(gMaterialList.monitor != NULL ? gMaterialList.monitor : TEXT("-")),
			TEXT("-"),
			TEXT("-"),
			(gMaterialList.monitorSize != NULL ? gMaterialList.monitorSize : TEXT("-")),
			_itow(prices[10], tempPrice, 10));
		writeLineIntoFile(fileIOSrings.monitorR);
		writeLineIntoFile(L"\n");

	}

	if (prices[11] != 0) {

		formatStringForFile(fileIOSrings.printerR,
			items[11],
			(gMaterialList.printerCompany != NULL ? gMaterialList.printerCompany : TEXT("-")),
			(gMaterialList.printer != NULL ? gMaterialList.printer : TEXT("-")),
			TEXT("-"),
			(gMaterialList.printerType != NULL ? gMaterialList.printerType : TEXT("-")),
			TEXT("-"),
			_itow(prices[11], tempPrice, 10));
		writeLineIntoFile(fileIOSrings.printerR);
		writeLineIntoFile(L"\n");

	}

		writeLineIntoFile(L"--------------------------------------------------------------------------------------------------------------------------------------\n");
		writeLineIntoFile(L"			                                        					TOTAL | Rs. ");
		writeLineIntoFile(_itow(totalPrice, tempPrice, 10));
		writeLineIntoFile(L"\n\r--------------------------------------------------------------------------------------------------------------------------------------\n");
		writeLineIntoFile(L"\n\n\n\n\r     Signature \n");
		writeLineIntoFile(L"\r Vivid Computer Shop \n");
	}
	totalPrice = 0;
	_fcloseall();

}

void writeLineIntoFile(TCHAR data[])
{
	gbErrorFlag = fputws(data, fp);
	if (FALSE == gbErrorFlag)
	{
		printf("Failed to write file.\n");
	}
}

void formatStringForFile(TCHAR *buffer, TCHAR *data1, TCHAR *data2, TCHAR *data3, TCHAR *data4, TCHAR *data5, TCHAR *data6, TCHAR *data7)
{
		// %-20s for proper alignment for in the text file
	    wsprintf(buffer, TEXT("\r| %-20s | %-15s | %-20s | %-15s | %-20s | %-10s | %-10s |"), data1, data2,
			data3, data4, data5, data6, data7);
}
