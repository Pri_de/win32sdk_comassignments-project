// CPU 101 & 102 kept reserved
#define ID_CBCPUCOMPANY 103
#define ID_CBCPUPROCESSOR 104
#define ID_CBCPUMODEL 105
#define ID_CBCPUGENERATION 106
#define ID_CBRAMCOMPANY 107
#define ID_CBRAMSIZE 108
#define ID_CBMOTHERBOSRD 109
#define ID_CBGCCOMPANY 110
#define ID_CBGCSIZE 111
#define ID_CBGCSERIAL 112
#define ID_CBHDCOMPANY 113
#define ID_CBHDSIZE 114
#define ID_CBCDDVDDRIVE 115
#define ID_CBSMPS 116
#define ID_CBCABINET 117
#define ID_CBKEYBOARD 118
#define ID_CBMOUSECOMPANY 119
#define ID_CBMONITORCOMPANY 120
#define ID_CBGCSERIAL1 121
#define ID_CBPRINTERCOMPANY 122
#define ID_CBMOTHERBOSRDCOMPANY 123
#define ID_CBCDDVDDRIVECOMPANY 124
#define ID_CBSMPSCOMPANY 125
#define ID_CBSMPSPOWER 126
#define ID_CBCABINETCOMPANY 127
#define ID_CBKEYBOARDCOMPANY 128
#define ID_CBKEYBOARDCOLOR 129
#define ID_CBMOUSETYPE 130
#define ID_CBMONITORSERIES 131
#define ID_CBMONITORSIZE 132
#define ID_CBPRINTERTYPE 133
#define ID_CBPRINTERCONNECT 134
#define ID_CBCABINETCOLOR 135
//resources
#define IDB_SPLASHID 200
#define IDB_BGID 201
// macros declaration 
#define WINWIDTH 1400
#define WINHEIGHT 800
#define SIZE_OF_POINTER 4
//callback functions
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
BOOL CALLBACK MyDialogProc(HWND, UINT, WPARAM, LPARAM);
BOOL CALLBACK MyReceiptDialogProc(HWND, UINT, WPARAM, LPARAM);
BOOL CALLBACK MyFileIODialogProc(HWND, UINT, WPARAM, LPARAM);
BOOL CALLBACK MyThankyouProc(HWND, UINT, WPARAM, LPARAM);
//structures
typedef struct userMaterialDetails {
	TCHAR *cpuCompany;
	TCHAR *cpuProcessor;
	TCHAR *cpuModel;
	TCHAR *cpuGeneration;
	TCHAR *ramCompany;
	TCHAR *ramSize;
	TCHAR *motherBoardCompany;
	TCHAR *motherBoard;
	TCHAR *gcCompany;
	TCHAR *gcSize;
	TCHAR *gcSerial;
	TCHAR *hdCompany;
	TCHAR *hdSize;
	TCHAR *cdDvdDriveCompany;
	TCHAR *cdDvdDrive;
	TCHAR *smpsCompany;
	TCHAR *smps;
	TCHAR *smpsPower;
	TCHAR *cabinetCompany;
	TCHAR *cabinet;
	TCHAR *cabinetColor;
	TCHAR *keyboardCompany;
	TCHAR *keyboardType;
	TCHAR *mouseCompany;
	TCHAR *mouseType;
	TCHAR *monitorCompany;
	TCHAR *monitor;
	TCHAR *monitorSize;
	TCHAR *printerCompany;
	TCHAR *printer;
	TCHAR *printerType;
}materialList;
materialList gMaterialList;

typedef struct receiptData {
	TCHAR headingR[255];
	TCHAR cpuCompanyR[255];
	TCHAR ramR[255];
	TCHAR motherBoardR[255];
	TCHAR gcCompanyR[255];
	TCHAR hdCompanyR[255];
	TCHAR cdDvdDriveR[255];
	TCHAR smpsR[255];
	TCHAR cabinetR[255];
	TCHAR keyboardR[255];
	TCHAR mouseR[255];
	TCHAR monitorR[255];
	TCHAR printerR[255];
}rData;
rData fileIOSrings;

//database
#pragma region HardwareDatabase

#pragma region CPUData
TCHAR *cpuCompany[] = { TEXT("--Select Option--"), TEXT("Intel"), TEXT("AMD") };
TCHAR *intelProcessor[] = { TEXT("--Select Option--"), TEXT("Core-i7"), TEXT("Core-i5") };
TCHAR *AMDProcessor[] = { TEXT("--Select Option--"), TEXT("Ryzen"), TEXT("Sempron") }; // add later

TCHAR *intelCoreI7Models[] = { TEXT("--Select Option--"), TEXT("8700U"), TEXT("8550"), TEXT("7740"), TEXT("7700T"), TEXT("7800"), TEXT("6785") }; // add later
TCHAR *intelCoreI5Models[] = { TEXT("--Select Option--"), TEXT("8600"), TEXT("8400"), TEXT("7640"), TEXT("7600"), TEXT("7500"), TEXT("7400"), TEXT("6585"), TEXT("6400") }; // add later
TCHAR *AMDRyzenModels[] = { TEXT("--Select Option--"), TEXT("Threadripper 1950X"), TEXT("7 Pro 1700X"), TEXT("5 Pro 1600X") }; // add later
TCHAR *AMDSempronModels[] = { TEXT("--Select Option--"),TEXT("3600"), TEXT("3500"), TEXT("3400"), TEXT("3200") }; // add later

TCHAR *intelCpuGenration[] = { TEXT("--Select Option--"), TEXT("Lynnfield"), TEXT("Clarkdale"),TEXT("Sandy Bridge"), TEXT("Ivy Bridge "),TEXT("Hasswell"), TEXT("Broadwell"), TEXT("Skylake"), TEXT("Kaby Lake"), TEXT("Coffee Lake") }; // add later
TCHAR *AMDCpuGenration[] = { TEXT("--Select Option--"),TEXT("Summit Ridge"), TEXT("Thoroughbred"),TEXT("Thorton"), TEXT("Barton"),TEXT("Paris"), TEXT("Palermo"), TEXT("Manila") }; // add later
#pragma endregion

#pragma region RAMData
TCHAR *RAMCompany[] = { TEXT("--Select Option--"), TEXT("GSkill"), TEXT("ADATA"), TEXT("IBM"), TEXT("Micron Technology") };
TCHAR *RAMSize[] = { TEXT("--Select Option--"),TEXT("8GB"), TEXT("16GB") };
#pragma endregion

#pragma region MotherBoardData
TCHAR *motherBoardCompany[] = { TEXT("--Select Option--"), TEXT("Asus"), TEXT("Gigabyte"), TEXT("Biostar") };
TCHAR *motherBoardAsus[] = { TEXT("--Select Option--"), TEXT("PRIME B250M-A"), TEXT("PRIME X299-A"), TEXT("ASUS H81M-CS")};
TCHAR *motherBoardGigabyte[] = { TEXT("--Select Option--"), TEXT("GA-Z270-Gaming"), TEXT("GA-Z170X-Ultra"), TEXT("Z370 AORUS")};
TCHAR *motherBoardBiostar[] = { TEXT("--Select Option--"), TEXT("Intel P45"), TEXT("Intel G41")};
#pragma endregion

#pragma region GraphicsCardData
TCHAR *graphicsCardCompany[] = { TEXT("--Select Option--"), TEXT("Intel"), TEXT("AMD"), TEXT("NVIDIA") };
TCHAR *graphicsCardSize[] = { TEXT("--Select Option--"), TEXT("4GB"),TEXT("8GB"), TEXT("16GB") };
TCHAR *graphicsIntelSerial[] = { TEXT("--Select Option--"), TEXT("IntelHD4000"), TEXT("IntelHD8000") }; // add later
TCHAR *graphicsAMDSerial[] = { TEXT("--Select Option--"), TEXT("RX Vega 64"),TEXT("R9 Fury X"), TEXT("RX 560"), TEXT("HD 7950") }; // add later
TCHAR *graphicsNVIDIASerial[] = { TEXT("--Select Option--"), TEXT("GTX 770 GTX 590"), TEXT("GTX 1050"), TEXT("GTX 1050 Ti"), TEXT("GTX 960"), TEXT("GTX 950") }; // add later
#pragma endregion

#pragma region HardDiskData
TCHAR *hardDiskCompany[] = { TEXT("--Select Option--"), TEXT("Seagate"), TEXT("WD") };
TCHAR *hardDiskSize[] = { TEXT("--Select Option--"), TEXT("500GB"),TEXT("1TB") };
#pragma endregion

#pragma region CD/DVDDriveData
TCHAR *CDDVDDriveCompany[] = { TEXT("--Select Option--"), TEXT("AMW"), TEXT("Astrad"), TEXT("Acros")}; //add later if required
TCHAR *CDDVDDriveAMW[] = { TEXT("--Select Option--"), TEXT("AMW S99 DVD"), TEXT("AMW Amphion V99"), TEXT("M280 Portable")};
TCHAR *CDDVDDriveAstrad[] = { TEXT("--Select Option--"), TEXT("Astrad S99"), TEXT("Astrad V99"), TEXT("Astrad M280"), TEXT("Astrad ultra") };
TCHAR *CDDVDDriveAcros[] = { TEXT("--Select Option--"), TEXT("Acros S99"), TEXT("Across V99"), TEXT("Acros M420") };
#pragma endregion

#pragma region SMPS
TCHAR *SMPSCompany[] = { TEXT("--Select Option--"), TEXT("Corsair"), TEXT("Antec"), TEXT("Sea Sonic") };
TCHAR *SMPSCorsair[] = { TEXT("--Select Option--"), TEXT("VS SERIES"), TEXT("CX SERIES"), TEXT("CS SERIES"), TEXT("RM SERIES") };
TCHAR *SMPSAntec[] = { TEXT("--Select Option--"), TEXT("Basiq"), TEXT("VP"), TEXT("EarthWatts") };
TCHAR *SMPSSea[] = { TEXT("--Select Option--"), TEXT("ATX"), TEXT("SFX"), TEXT("TFX") };
TCHAR *SMPSPower[] = { TEXT("--Select Option--"), TEXT("650W"), TEXT("450W"), TEXT("350W") };
#pragma endregion

#pragma region CabinetData
TCHAR *cabinetCompany[] = { TEXT("--Select Option--"), TEXT("Antec"), TEXT("Circle"), TEXT("Corsair") };
TCHAR *cabinetAntec[] = { TEXT("--Select Option--"), TEXT("GX200"), TEXT("900"), TEXT("X1") };
TCHAR *cabinetCircle[] = { TEXT("--Select Option--"), TEXT("CC819"), TEXT("CC890"), TEXT("Pheonix") };
TCHAR *cabinetCorsair[] = { TEXT("--Select Option--"), TEXT("Carbide 2100"), TEXT("Carbide 432"), TEXT("Carbide 900") };
TCHAR *cabinetColor[] = { TEXT("--Select Option--"), TEXT("Black"), TEXT("Gray"), TEXT("Multicolor") };
#pragma endregion

#pragma region KeyboardData
TCHAR *keyboardCompany[] = { TEXT("--Select Option--"), TEXT("Microsoft"), TEXT("Logitek"), TEXT("Lenovo") };
TCHAR *keyboardType[] = { TEXT("--Select Option--"), TEXT("USB"), TEXT("Wireless")};
#pragma endregion

#pragma region MouseData
TCHAR *mouseCompany[] = { TEXT("--Select Option--"), TEXT("Microsoft"), TEXT("Logitek"), TEXT("Lenovo") };
TCHAR *mouseType[] = { TEXT("--Select Option--"), TEXT("USB"), TEXT("Wireless") };
#pragma endregion

#pragma region MonitorData
TCHAR *monitorCompany[] = { TEXT("--Select Option--"), TEXT("BenQ"), TEXT("AOC Agon"), TEXT("HP"), TEXT("LG") };
TCHAR *monitorBenQ[] = { TEXT("--Select Option--"), TEXT("BenQ GW"), TEXT("BenQ PD"), TEXT("BenQ BL")};
TCHAR *monitorAOC[] = { TEXT("--Select Option--"), TEXT("AOC Agon AG"), TEXT("AOC Agon G")};
TCHAR *monitorHP[] = { TEXT("--Select Option--"), TEXT("HP Compaq"), TEXT("HP Pavillion")};
TCHAR *monitorLG[] = { TEXT("--Select Option--"), TEXT("LG MP"), TEXT("LG MN"), TEXT("LG GM")};
TCHAR *monitorSize[] = { TEXT("--Select Option--"), TEXT("20\""), TEXT("24\""), TEXT("27\""), TEXT("34\"")};
#pragma endregion

#pragma region PrinterData
TCHAR *printerCompany[] = { TEXT("--Select Option--"), TEXT("HP"), TEXT("Epson"), TEXT("Canon")}; //add later if required
TCHAR *printerHP[] = { TEXT("--Select Option--"), TEXT("DeskJet 2131"), TEXT("Advantage 2135"), TEXT("LaserJet M1005")};
TCHAR *printerEpson[] = { TEXT("--Select Option--"), TEXT("L360"), TEXT("L380")};
TCHAR *printerCanon[] = { TEXT("--Select Option--"), TEXT("Pixma MG2577s"), TEXT("Pixma E477"), TEXT("LaserJet M1005") };
TCHAR *printerConnect[] = { TEXT("--Select Option--"), TEXT("USB"), TEXT("Wireless") };
#pragma endregion

#pragma endregion

//variables
TCHAR *headings[] = { TEXT("DEVICE"), TEXT("COMPANY"), TEXT("SUBTYPE"), TEXT("GENERATION"), TEXT("MODEL"), TEXT("SIZE"), TEXT("TOTAL") };
TCHAR *items[] = { TEXT("01]  CPU"),
TEXT("02]  RAM"),
TEXT("03]  Motherboard"),
TEXT("04]  Graphics Card"),
TEXT("05]  Hard disks"),
TEXT("06]  CD/DVD Drive"),
TEXT("07]  SMPS"),
TEXT("08]  Cabinet"),
TEXT("09]  Keyboard"),
TEXT("10]  Mouse"),
TEXT("11]  Monitor"),
TEXT("12]  Printer")};
TCHAR *temp[] = { TEXT("-"),
TEXT("-"),
TEXT("-"),
TEXT("-"),
TEXT("-"),
TEXT("-"),
TEXT("-"),
TEXT("-"),
TEXT("-"),
TEXT("-"),
TEXT("-"),
TEXT("-")};


RECT rc;
HINSTANCE hInst;
HWND ghDlg = NULL, gHwnd;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
TCHAR name[50];
TCHAR str[255];
TCHAR tempPrice[20];
HANDLE hFp = NULL;
FILE *fp;
DWORD gdwBytesWritten = 0;
BOOL gbErrorFlag = FALSE;
BOOL receiptSavedFlag = FALSE;
static WORD gwInvoiceNumber = 1200;
static HBITMAP hBitmap;
bool splashScreenActive = true;
int giSelect;
int counter = 0;
int totalPrice;
int prices[12];
int i = 0, X = 0, Y = 0;
