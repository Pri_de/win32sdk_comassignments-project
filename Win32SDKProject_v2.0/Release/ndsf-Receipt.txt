**************************************Vivid Computer Shop*****************************************



--------------------------------------------------------------------------------------------------
  Name : ndsf                                                     Invoice No : 1201 

--------------------------------------------------------------------------------------------------
DEVICE | COMPANY | SUBTYPE | GENERATION | MODEL | SIZE | TOTAL

01]  CPU | AMD | Sempron | Thoroughbred | 3500 | - | 16000

02]  RAM | ADATA | - | - | - | 16GB | 12200

03]  Motherboard | Intel DH61WW | - | - | - | - | 8290

04]  Graphics Card | AMD | RX Vega 64 | - | - | 8GB | 8800

05]  Hard disks | Seagate | - | - | - | 1TB | 3127

06]  CD/DVD Drive | Samsung DVD-RW 22X SATA DL | - | - | - | - | 2478

07]  SMPS | Corsair AX-860H | - | - | - | - | 3460

08]  Cabinet | Zebronics | - | - | - | - | 1420

09]  Keyboard | Logitek | - | - | - | - | 3460

10]  Mouse | Logitek | - | - | - | - | 250

11]  Monitor | BenQ PD3200U 4K | - | - | - | - | 5490

-----------------------------------------------------------------------------------------------
                                              TOTAL | Rs. 61915