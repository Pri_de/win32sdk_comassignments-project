//#define UNICODE
#include <Windows.h>
#include <stdio.h>
#include <strsafe.h>
#include <tchar.h>
#include "Win32SDKDialog.h"
#include "Receipt.h"

int WINAPI WinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR inCmdLine,
	int nCmdShow)
{
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR AppName[] = TEXT("Window");

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpszClassName = AppName;
	wndclass.lpszMenuName = NULL;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndclass.hIcon = LoadIcon(hInstance, TEXT("IDI_ICON"));
	wndclass.hIconSm = LoadIcon(hInstance, TEXT("IDI_ICON"));
	wndclass.hCursor = LoadCursor(hInstance, TEXT("IDI_ICON"));

	RegisterClassEx(&wndclass);

	hwnd = CreateWindow(AppName,
		TEXT("First Window"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0, //CW_USEDEFAULT,
		0, //CW_USEDEFAULT,
		WINWIDTH, // CW_USEDEFAULT,
		WINHEIGHT, // CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);
	gHwnd = hwnd;
	if (hwnd == NULL)
	{
		MessageBox(hwnd, TEXT("Window not created"), TEXT("Error.."), MB_OK);
		exit(0);
	}

	ShowWindow(hwnd, SW_MAXIMIZE);
	UpdateWindow(hwnd);

	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return ((int)msg.wParam);
}


LRESULT CALLBACK WndProc(HWND hwnd,
	UINT iMsg,
	WPARAM wParam,
	LPARAM lParam)
{

	bool paintMainWindow(void);


	switch (iMsg)
	{
	
	case WM_CREATE:
		//DialogBox(hInst, TEXT("DATAENTRY"), hwnd, MyDialogProc);
		break;

	case WM_SIZE:
	    InvalidateRect(hwnd, NULL, FALSE);
		break;

	case WM_PAINT:
		paintMainWindow();		
		break;
		
	//Dialogue Destroy notification to main/parent window
	case WM_PARENTNOTIFY:
		if (LOWORD(wParam) == 0x0002) {
			splashScreenActive = false;
		}
		break;
	
	case WM_DESTROY:		
		PostQuitMessage(0);
		break;

	case WM_KEYDOWN:
		switch (wParam) {
		case VK_SPACE:
			hInst = (HINSTANCE)GetWindowLong(hwnd, GWLP_HINSTANCE);
			DialogBox(hInst, TEXT("DATAENTRY"), hwnd, MyDialogProc);
			break;

		case VK_ESCAPE:
			if (!splashScreenActive) {
				hInst = (HINSTANCE)GetWindowLong(hwnd, GWLP_HINSTANCE);
				DialogBox(hInst, TEXT("THANKYOU"), hwnd, MyThankyouProc);
			}
			PostQuitMessage(0);
			break;
			
		case 'P':
			hInst = (HINSTANCE)GetWindowLong(hwnd, GWLP_HINSTANCE);
			DialogBox(hInst, TEXT("RECEIPT"), hwnd, MyReceiptDialogProc);
			break;

		default:
			break;
		}
		break;

	default:
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}



BOOL CALLBACK MyThankyouProc(HWND hDlg, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	switch (iMsg)
	{
	case WM_INITDIALOG:
		return(TRUE);

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDOK:
			EndDialog(hDlg, 0);
			break;

		}
		return TRUE;
	}

	return FALSE;

}

BOOL CALLBACK MyDialogProc(HWND hDlg, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	bool initializeDialogBox(void);
	void enableIntelProcessorCB(void);
	void enableAMDProcessorCB(void);
	void enableI7ModelCB(void);
	void enableI5ModelCB(void);
	void enableRyzenModelCB(void);
	void enableSempronModelCB(void);
	void enableIntelGenCB(void);
	void enableAMDGenCB(void);
	void enableRAMSizeCB(void);
	void enableIntelGCSerialCB(void);
	void enableAMDGCSerialCB(void);
	void enableNVIDIAGCSerialCB(void);
	void enableGCSizeSerialCB(void);
	void enableHDSizeCB(void);

	HDC hdcInst, hdcBitmap;
	PAINTSTRUCT ps;
	BITMAP bp;
	RECT r;
	HINSTANCE hInst;
	HBITMAP hBitmap; //bitmap object to hold your bitmap
	switch (iMsg)
	{
	case WM_INITDIALOG:
		
		ghDlg = hDlg;
		SetFocus(GetDlgItem(hDlg, ID_CBCPUCOMPANY));
		//fill all combo-boxes with data
		initializeDialogBox();	
		return(TRUE);

	case WM_PAINT:
		hdcInst = BeginPaint(hDlg, &ps);
		hInst = (HINSTANCE)GetWindowLong(hDlg, GWLP_HINSTANCE);
		hBitmap = LoadBitmap(hInst, MAKEINTRESOURCE(IDB_BGID));
		hdcBitmap = CreateCompatibleDC(hdcInst);
		GetClientRect(hDlg, &r);
		SelectObject(hdcBitmap, hBitmap);
		GetObject(hBitmap, sizeof(bp), &bp);
		BitBlt(hdcInst, 0, 0, r.right - r.left, r.bottom - r.top, hdcBitmap, 0, 0, SRCCOPY);
		DeleteDC(hdcBitmap);
		EndPaint(hDlg, &ps);
		break;

	case WM_CTLCOLORSTATIC:
		hdcInst = (HDC)wParam;
		SetTextColor(hdcInst, RGB(255, 255, 255));
		SetBkColor(hdcInst, TRANSPARENT);
		SetBkMode(hdcInst, TRANSPARENT);
		return (LRESULT)GetStockObject(NULL_BRUSH);
	
	case WM_COMMAND:

		switch (LOWORD(wParam))
		{
		
			case IDOK:
				//Here change the size of the window for further painting of material list
				GetWindowPlacement(gHwnd, &wpPrev);
				wpPrev.rcNormalPosition.top = wpPrev.rcNormalPosition.top + 0x50;
				wpPrev.rcNormalPosition.bottom = wpPrev.rcNormalPosition.bottom - 0x50;
				wpPrev.rcNormalPosition.left = wpPrev.rcNormalPosition.left + 0x50;
				wpPrev.rcNormalPosition.right = wpPrev.rcNormalPosition.right - 0x50;
				SetWindowPlacement(gHwnd, &wpPrev);
				splashScreenActive = false;
				InvalidateRect(gHwnd, NULL, true);
				EndDialog(hDlg, 0);
				break;
			case IDCANCEL:
				EndDialog(hDlg, 0);
				break;
			default:
				break;
		}
	
	
		switch (HIWORD(wParam))
		{
		case CBN_SELCHANGE:
			switch (LOWORD(wParam))
			{

			case ID_CBCPUCOMPANY:

				giSelect = SendDlgItemMessage(hDlg, ID_CBCPUCOMPANY, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

				//GetDia
				switch (giSelect) {
				case 0:
					ShowWindow(GetDlgItem(ghDlg, ID_CBCPUPROCESSOR), SW_HIDE);
					ShowWindow(GetDlgItem(ghDlg, ID_CBCPUMODEL), SW_HIDE);
					ShowWindow(GetDlgItem(ghDlg, ID_CBCPUGENERATION), SW_HIDE);
					gMaterialList.cpuCompany = NULL;
					gMaterialList.cpuProcessor = NULL;
					gMaterialList.cpuModel = NULL;
					gMaterialList.cpuGeneration = NULL;
					break;

				case 1:
					SendDlgItemMessage(hDlg, ID_CBCPUPROCESSOR, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
					enableIntelProcessorCB();
					ShowWindow(GetDlgItem(ghDlg, ID_CBCPUPROCESSOR), SW_SHOW);
					ShowWindow(GetDlgItem(ghDlg, ID_CBCPUMODEL), SW_HIDE);
					ShowWindow(GetDlgItem(ghDlg, ID_CBCPUGENERATION), SW_HIDE);
					gMaterialList.cpuCompany = cpuCompany[giSelect];
					break;

				case 2:
					SendDlgItemMessage(hDlg, ID_CBCPUPROCESSOR, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
					enableAMDProcessorCB();
					ShowWindow(GetDlgItem(ghDlg, ID_CBCPUPROCESSOR), SW_SHOW);
					ShowWindow(GetDlgItem(ghDlg, ID_CBCPUMODEL), SW_HIDE);
					ShowWindow(GetDlgItem(ghDlg, ID_CBCPUGENERATION), SW_HIDE);
					gMaterialList.cpuCompany = cpuCompany[giSelect];
					break;

				}
				break;

			case ID_CBCPUPROCESSOR:

				giSelect = SendDlgItemMessage(hDlg, ID_CBCPUPROCESSOR, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

				if (!wcscmp(gMaterialList.cpuCompany, TEXT("Intel"))) {

					switch (giSelect) {
					case 0:
						ShowWindow(GetDlgItem(ghDlg, ID_CBCPUMODEL), SW_HIDE);
						ShowWindow(GetDlgItem(ghDlg, ID_CBCPUGENERATION), SW_HIDE);
						gMaterialList.cpuProcessor = NULL;
						gMaterialList.cpuModel = NULL;
						gMaterialList.cpuGeneration = NULL;
						prices[0] = 0;
						break;

					case 1:
						SendDlgItemMessage(hDlg, ID_CBCPUMODEL, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
						enableI5ModelCB();
						ShowWindow(GetDlgItem(ghDlg, ID_CBCPUMODEL), SW_SHOW);
						ShowWindow(GetDlgItem(ghDlg, ID_CBCPUGENERATION), SW_HIDE);
						gMaterialList.cpuProcessor = intelProcessor[giSelect];
						prices[0] = 14000;
						break;

					case 2:
						SendDlgItemMessage(hDlg, ID_CBCPUMODEL, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
						enableI7ModelCB();
						ShowWindow(GetDlgItem(ghDlg, ID_CBCPUMODEL), SW_SHOW);
						ShowWindow(GetDlgItem(ghDlg, ID_CBCPUGENERATION), SW_HIDE);
						gMaterialList.cpuProcessor = intelProcessor[giSelect];
						prices[0] = 22500;
						break;
					}
				}
				else if (!wcscmp(gMaterialList.cpuCompany, TEXT("AMD")))
				{
					switch (giSelect) {
					case 0:
						ShowWindow(GetDlgItem(ghDlg, ID_CBCPUMODEL), SW_HIDE);
						ShowWindow(GetDlgItem(ghDlg, ID_CBCPUGENERATION), SW_HIDE);
						gMaterialList.cpuProcessor = NULL;
						gMaterialList.cpuModel = NULL;
						gMaterialList.cpuGeneration = NULL;
						break;

					case 1:
						SendDlgItemMessage(hDlg, ID_CBCPUMODEL, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
						enableRyzenModelCB();
						ShowWindow(GetDlgItem(ghDlg, ID_CBCPUMODEL), SW_SHOW);
						ShowWindow(GetDlgItem(ghDlg, ID_CBCPUGENERATION), SW_HIDE);
						gMaterialList.cpuProcessor = AMDProcessor[giSelect];
						prices[0] = 12500;
						break;

					case 2:
						SendDlgItemMessage(hDlg, ID_CBCPUMODEL, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
						enableSempronModelCB();
						ShowWindow(GetDlgItem(ghDlg, ID_CBCPUMODEL), SW_SHOW);
						ShowWindow(GetDlgItem(ghDlg, ID_CBCPUGENERATION), SW_HIDE);
						gMaterialList.cpuProcessor = AMDProcessor[giSelect];
						prices[0] = 16000;
						break;
					}

				}
				break;

			case ID_CBCPUMODEL:

				giSelect = SendDlgItemMessage(hDlg, ID_CBCPUMODEL, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

				if (giSelect == 0)
				{
					ShowWindow(GetDlgItem(ghDlg, ID_CBCPUGENERATION), SW_HIDE);
					gMaterialList.cpuModel = NULL;
					gMaterialList.cpuGeneration = NULL;
					//break;
				}
				else
				{
					SendDlgItemMessage(hDlg, ID_CBCPUGENERATION, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
					if (!wcscmp(gMaterialList.cpuCompany, TEXT("Intel")))
					{
						enableIntelGenCB();
						if (!wcscmp(gMaterialList.cpuProcessor, TEXT("Core-i7")))
							gMaterialList.cpuModel = intelCoreI7Models[giSelect];
						else
							gMaterialList.cpuModel = intelCoreI5Models[giSelect];

					}
					else if (!wcscmp(gMaterialList.cpuCompany, TEXT("AMD")))
					{
						enableAMDGenCB();
						if (!wcscmp(gMaterialList.cpuProcessor, TEXT("Ryzen")))
							gMaterialList.cpuModel = AMDRyzenModels[giSelect];
						else
							gMaterialList.cpuModel = AMDSempronModels[giSelect];
					}

					ShowWindow(GetDlgItem(ghDlg, ID_CBCPUGENERATION), SW_SHOW);
				}
				break;


			case ID_CBCPUGENERATION:

				giSelect = SendDlgItemMessage(ghDlg, ID_CBCPUGENERATION, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

				if (giSelect != 0)
				{
					if (!wcscmp(gMaterialList.cpuCompany, TEXT("Intel")))
					{
						gMaterialList.cpuGeneration = intelCpuGenration[giSelect];
					}
					else if (!wcscmp(gMaterialList.cpuCompany, TEXT("AMD")))
					{
						gMaterialList.cpuGeneration = AMDCpuGenration[giSelect];
					}

				}

				break;


			case ID_CBRAM:
				giSelect = SendDlgItemMessage(ghDlg, ID_CBRAM, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

				if (giSelect != 0) {
					SendDlgItemMessage(hDlg, ID_CBRAMSIZE, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
					ShowWindow(GetDlgItem(ghDlg, ID_CBRAMSIZE), SW_SHOW);
					enableRAMSizeCB();
					gMaterialList.ram = RAMName[giSelect];
					prices[1] = 6500;
				}
				else
				{
					ShowWindow(GetDlgItem(ghDlg, ID_CBRAMSIZE), SW_HIDE);
				}

				break;

			case ID_CBRAMSIZE:
				giSelect = SendDlgItemMessage(ghDlg, ID_CBRAMSIZE, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);
				if (giSelect != 0) {
					gMaterialList.ramSize = RAMSize[giSelect];
					if (RAMSize[giSelect] == TEXT("8GB"))
						prices[1] = 6500;
					else
						prices[1] = 12200;
				}

				break;

			case ID_CBMOTHERBOSRD:
				giSelect = SendDlgItemMessage(ghDlg, ID_CBMOTHERBOSRD, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

				if (giSelect != 0) {
					gMaterialList.motherBoard = motherBoard[giSelect];
					prices[2] = 8290;

				}

				break;

			case ID_CBGCCOMPANY:
				giSelect = SendDlgItemMessage(hDlg, ID_CBGCCOMPANY, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);
				ShowWindow(GetDlgItem(ghDlg, ID_CBGCSERIAL1), SW_HIDE);
				ShowWindow(GetDlgItem(ghDlg, ID_CBGCSIZE), SW_HIDE);
				switch (giSelect)
				{
				case 0:
					gMaterialList.gcCompany = NULL;
					gMaterialList.gcSerial = NULL;
					gMaterialList.gcSize = NULL;
					break;

				case 1:
					SendDlgItemMessage(hDlg, ID_CBGCSERIAL1, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
					enableIntelGCSerialCB();
					ShowWindow(GetDlgItem(ghDlg, ID_CBGCSERIAL1), SW_SHOW);
					gMaterialList.gcCompany = graphicsCard[giSelect];
					gMaterialList.gcSize = NULL;
					break;

				case 2:
					SendDlgItemMessage(hDlg, ID_CBGCSERIAL1, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
					enableAMDGCSerialCB();
					ShowWindow(GetDlgItem(ghDlg, ID_CBGCSERIAL1), SW_SHOW);
					gMaterialList.gcCompany = graphicsCard[giSelect];
					gMaterialList.gcSize = NULL;
					break;


				case 3:
					SendDlgItemMessage(hDlg, ID_CBGCSERIAL1, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
					enableNVIDIAGCSerialCB();
					ShowWindow(GetDlgItem(ghDlg, ID_CBGCSERIAL1), SW_SHOW);
					gMaterialList.gcCompany = graphicsCard[giSelect];
					gMaterialList.gcSize = NULL;
					break;
				}
				break;

			case ID_CBGCSERIAL1:
				giSelect = SendDlgItemMessage(hDlg, ID_CBGCSERIAL1, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);
				if (giSelect != 0)
				{
					SendDlgItemMessage(hDlg, ID_CBGCSIZE, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
					enableGCSizeSerialCB();
					ShowWindow(GetDlgItem(ghDlg, ID_CBGCSIZE), SW_SHOW);

					if (!wcscmp(gMaterialList.gcCompany, TEXT("Intel"))) {
						gMaterialList.gcSerial = graphicsIntelSerial[giSelect];
					}
					else if (!wcscmp(gMaterialList.gcCompany, TEXT("AMD"))) {
						gMaterialList.gcSerial = graphicsAMDSerial[giSelect];
						prices[3] = 8800;
					}
					else {
						gMaterialList.gcSerial = graphicsNVIDIASerial[giSelect];
						prices[3] = 12800;
					}
				}
				break;

			case ID_CBGCSIZE:
				giSelect = SendDlgItemMessage(hDlg, ID_CBGCSIZE, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

				if (giSelect != 0)
				{
					gMaterialList.gcSize = graphicsCardSize[giSelect];
				}
				break;

			case ID_CBHDCOMPANY:
				giSelect = SendDlgItemMessage(ghDlg, ID_CBHDCOMPANY, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

				if (giSelect != 0) {
					SendDlgItemMessage(hDlg, ID_CBHDSIZE, CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
					ShowWindow(GetDlgItem(ghDlg, ID_CBHDSIZE), SW_SHOW);
					enableHDSizeCB();
					gMaterialList.hdCompany = hardDiskCompany[giSelect];
					if (giSelect == 1)
						prices[4] = 3127;
					else
						prices[4] = 2898;
				}
				else
				{
					ShowWindow(GetDlgItem(ghDlg, ID_CBHDSIZE), SW_HIDE);
				}

				break;

			case ID_CBHDSIZE:
				giSelect = SendDlgItemMessage(ghDlg, ID_CBRAMSIZE, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

				if (giSelect != 0)
					gMaterialList.hdSize = hardDiskSize[giSelect];
				break;

			case ID_CBCDDVDDRIVE:
				giSelect = SendDlgItemMessage(ghDlg, ID_CBCDDVDDRIVE, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

				if (giSelect != 0) {
					gMaterialList.cdDvdDrive = CDDVDDrive[giSelect];
					prices[5] = 2478;
				}
				break;

			case ID_CBSMPS:
				giSelect = SendDlgItemMessage(ghDlg, ID_CBSMPS, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

				if (giSelect != 0) {
					gMaterialList.smps = SMPS[giSelect];
					prices[6] = 3460;
				}

				break;

			case ID_CBCABINET:
				giSelect = SendDlgItemMessage(ghDlg, ID_CBCABINET, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

				if (giSelect != 0) {
					gMaterialList.cabinet = cabinet[giSelect];
					prices[7] = 1420;
				}

				break;

			case ID_CBKEYBOARD:
				giSelect = SendDlgItemMessage(ghDlg, ID_CBKEYBOARD, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

				if (giSelect != 0) {
					gMaterialList.keyboard = keyboard[giSelect];
					prices[8] = 400;
				}

				break;

			case ID_CBMOUSECOMPANY:
				giSelect = SendDlgItemMessage(ghDlg, ID_CBMOUSECOMPANY, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

				if (giSelect != 0) {
					gMaterialList.mouse = mouse[giSelect];
					prices[9] = 250;
				}

				break;

			case ID_CBMONITORCOMPANY:
				giSelect = SendDlgItemMessage(ghDlg, ID_CBMONITORCOMPANY, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

				if (giSelect != 0) {
					gMaterialList.monitor = monitor[giSelect];
					prices[10] = 5490;
				}

				break;

			default:
				MessageBox(ghDlg, TEXT("YOU ARE IN CBN_SELCHANGE DEFAULT"), TEXT("ERROR"), MB_OK);
				break;
			}

			break;
		}
		return TRUE;

	}

	return FALSE;

}


BOOL CALLBACK MyReceiptDialogProc(HWND hDlg, UINT iMsg, WPARAM wParam, LPARAM lParam) {

	void writeReceiptFile(TCHAR *);
	
	switch (iMsg)
	{
	case WM_INITDIALOG:
		SetFocus(GetDlgItem(hDlg, ID_ETNAME));
		
		return(TRUE);


	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case ID_PBCONTINUE:
			EndDialog(hDlg, 0);
			break;

		case IDOK:
			GetDlgItemText(hDlg, ID_ETNAME, name, 50);
			writeReceiptFile(name);
			receiptSavedFlag = true;
			DialogBox(hInst, TEXT("FILEIO"), gHwnd, MyFileIODialogProc);
			EndDialog(hDlg, 0);
			break;

		case IDCANCEL:
			EndDialog(hDlg, 0);
			break;
		}

		return TRUE;

	}

	return FALSE;
}


BOOL CALLBACK MyFileIODialogProc(HWND hDlg, UINT iMsg, WPARAM wParam, LPARAM lParam) {

	void writeReceiptFile(TCHAR *);
	switch (iMsg)
	{
	case WM_INITDIALOG:
		return(TRUE);


	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDOK:
			InvalidateRect(gHwnd, &rc, true);
			EndDialog(hDlg, 0);
			break;

		case IDCANCEL:
			EndDialog(hDlg, 0);
			break;
		}

		return TRUE;

	}

	return FALSE;
}


bool paintMainWindow(void) {

	void refillTempArray(void);
	void formatStringForFile(TCHAR *buffer, TCHAR *data1, TCHAR *data2, TCHAR *data3, TCHAR *data4, TCHAR *data5, TCHAR *data6, TCHAR *data7);

	HDC hdcInst, hdcBitmap;
	PAINTSTRUCT ps;
	BITMAP bp;
	RECT r;
	HBRUSH hBrush;
	HPEN hPen;
	SIZE sz;
	int paraMargin = 0, resetMargin;
	int c;
	
	hdcInst = BeginPaint(gHwnd, &ps);
	if (splashScreenActive) {


		hInst = (HINSTANCE)GetWindowLong(gHwnd, GWLP_HINSTANCE);
		hBitmap = LoadBitmap(hInst, MAKEINTRESOURCE(IDB_SPLASHID));
		// Create a memory device compatible with the above DC variable
		hdcBitmap = CreateCompatibleDC(hdcInst);
		// Select the new bitmap
		SelectObject(hdcBitmap, hBitmap);
		GetObject(hBitmap, sizeof(bp), &bp);
		// Get client coordinates for the StretchBlt() function
		GetClientRect(gHwnd, &r);
		// stretch bitmap across client area
		StretchBlt(hdcInst, 0, 0, r.right - r.left, r.bottom - r.top, hdcBitmap, 0,
			0, bp.bmWidth, bp.bmHeight, SRCCOPY);

		//Title on material page
		SelectObject(hdcInst, CreateFont(40, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, ANSI_CHARSET,
			OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE,
			TEXT("Broadway")));
		SetTextColor(hdcInst, RGB(150, 0, 0));
		SetBkColor(hdcInst, TRANSPARENT);
		SetBkMode(hdcInst, TRANSPARENT);
		TextOut(hdcInst, 100, 500, TEXT("Welcome to Vivid Computer Shop...."), 32);

		SelectObject(hdcInst, CreateFont(20, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, ANSI_CHARSET,
			OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE,
			TEXT("Times New Roman")));
		SetTextColor(hdcInst, RGB(255, 255, 255));
		SetBkColor(hdcInst, TRANSPARENT);
		SetBkMode(hdcInst, TRANSPARENT);
		TextOut(hdcInst, 100, 550, TEXT("Hit Spacebar to continue...."), 28);
		

		// Cleanup
		DeleteDC(hdcBitmap);

	}
	else
	{
		GetClientRect(gHwnd, &rc);
		hBrush = CreateSolidBrush((COLORREF)RGB(0, 0, 0));
		FillRect(hdcInst, &ps.rcPaint, hBrush);
		//hPen = CreatePen(PS_SOLID, 3, (COLORREF)RGB(255, 255, 0));
		hPen = CreatePen(PS_SOLID, 3, (COLORREF)RGB(100, 100, 100));
		SelectObject(hdcInst, hPen);
		//Title section
		MoveToEx(hdcInst, 0, ps.rcPaint.top + 0x50, NULL);
		LineTo(hdcInst, ps.rcPaint.right, ps.rcPaint.top + 0x50);
		MoveToEx(hdcInst, 0, ps.rcPaint.top + 0x75, NULL);
		LineTo(hdcInst, ps.rcPaint.right, ps.rcPaint.top + 0x75);
		//total section
		MoveToEx(hdcInst, 0, ps.rcPaint.top + 0x1af, NULL);
		LineTo(hdcInst, ps.rcPaint.right, ps.rcPaint.top + 0x1af);
		MoveToEx(hdcInst, 0, ps.rcPaint.top + 0x1d5, NULL);
		LineTo(hdcInst, ps.rcPaint.right, ps.rcPaint.top + 0x1d5);
		//cloumns
		MoveToEx(hdcInst, ps.rcPaint.top + 0xa0, ps.rcPaint.top + 0x50, NULL);
		LineTo(hdcInst, ps.rcPaint.top + 0xa0, ps.rcPaint.top + 0x1af);
		MoveToEx(hdcInst, ps.rcPaint.top + 0x1b2, ps.rcPaint.top + 0x50, NULL);
		LineTo(hdcInst, ps.rcPaint.top + 0x1b2, ps.rcPaint.top + 0x1af);
		MoveToEx(hdcInst, ps.rcPaint.top + 0x210, ps.rcPaint.top + 0x50, NULL);
		LineTo(hdcInst, ps.rcPaint.top + 0x210, ps.rcPaint.top + 0x1af);
		MoveToEx(hdcInst, ps.rcPaint.top + 0x295, ps.rcPaint.top + 0x50, NULL);
		LineTo(hdcInst, ps.rcPaint.top + 0x295, ps.rcPaint.top + 0x1af);
		MoveToEx(hdcInst, ps.rcPaint.top + 0x345, ps.rcPaint.top + 0x50, NULL);
		LineTo(hdcInst, ps.rcPaint.top + 0x345, ps.rcPaint.top + 0x1af);
		MoveToEx(hdcInst, ps.rcPaint.top + 0x390, ps.rcPaint.top + 0x50, NULL);
		LineTo(hdcInst, ps.rcPaint.top + 0x390, ps.rcPaint.top + 0x1d5);
		MoveToEx(hdcInst, ps.rcPaint.top + 0x470, ps.rcPaint.top + 0x50, NULL);
		LineTo(hdcInst, ps.rcPaint.top + 0x470, ps.rcPaint.top + 0x1af);
		
		SelectObject(hdcInst, CreateFont(20, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, ANSI_CHARSET,
			OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE,
			TEXT("Cambria Bold")));
		SetTextColor(hdcInst, RGB(0, 255, 100));
		SetBkColor(hdcInst, TRANSPARENT);
		SetBkMode(hdcInst, TRANSPARENT);

		// Add column headings		
		TextOut(hdcInst, ps.rcPaint.top + 0x2A, ps.rcPaint.top + 0x5b, headings[0], wcslen(headings[0]));
		TextOut(hdcInst, ps.rcPaint.top + 0xEF, ps.rcPaint.top + 0x5b, headings[1], wcslen(headings[1]));
		TextOut(hdcInst, ps.rcPaint.top + 0x1BA, ps.rcPaint.top + 0x5b, headings[2], wcslen(headings[2]));
		TextOut(hdcInst, ps.rcPaint.top + 0x21C, ps.rcPaint.top + 0x5b, headings[3], wcslen(headings[3]));
		TextOut(hdcInst, ps.rcPaint.top + 0x2C5, ps.rcPaint.top + 0x5b, headings[4], wcslen(headings[4]));
		TextOut(hdcInst, ps.rcPaint.top + 0x355, ps.rcPaint.top + 0x5b, headings[5], wcslen(headings[5]));
		TextOut(hdcInst, ps.rcPaint.top + 0x3E0, ps.rcPaint.top + 0x5b, headings[6], wcslen(headings[6]));
		TextOut(hdcInst, ps.rcPaint.top + 0x310, ps.rcPaint.top + 0x1B7, headings[6], wcslen(headings[6]));

		formatStringForFile(fileIOSrings.headingR, headings[0], headings[1], headings[2], headings[3], headings[4],
			headings[5], headings[6]);
	
		SelectObject(hdcInst, CreateFont(20, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, ANSI_CHARSET,
			OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE,
			TEXT("Arial Bold")));
		SetTextColor(hdcInst, RGB(255, 0, 127));
		GetTextExtentPoint32(hdcInst, headings[0], wcslen(headings[0]), &sz);

		for (c = 0; c < 11; c++) {

			TextOut(hdcInst, ps.rcPaint.top + 0x5, ps.rcPaint.top + 0x85 +paraMargin, items[c], wcslen(items[c]));
			paraMargin += sz.cy + 7;
		}
		
		paraMargin = ps.rcPaint.top + 0x85;
		resetMargin = paraMargin;
		//fill device column
		temp[0] = gMaterialList.cpuCompany;
		temp[1] = gMaterialList.ram;
		temp[2] = gMaterialList.motherBoard;
		temp[3] = gMaterialList.gcCompany;
		temp[4] = gMaterialList.hdCompany;
		temp[5] = gMaterialList.cdDvdDrive;
		temp[6] = gMaterialList.smps;
		temp[7] = gMaterialList.cabinet;
		temp[8] = gMaterialList.keyboard;
		temp[9] = gMaterialList.mouse;
		temp[10] = gMaterialList.monitor;
		
		for (c = 0; c < 11; c++) {

			if (temp[c] != NULL)
				TextOut(hdcInst, 0xaa, paraMargin, temp[c], wcslen(temp[c]));
			else
				TextOut(hdcInst, 0xaa, paraMargin, TEXT("-"), 1);

			paraMargin += sz.cy + 7;
		}
		//refill temp array for subtype column
		refillTempArray();
		paraMargin = resetMargin;
		if(gMaterialList.cpuProcessor != NULL)
			temp[0] = gMaterialList.cpuProcessor;
		for (c = 0; c < 11; c++) {

				TextOut(hdcInst, 0x1ba, paraMargin, temp[c], wcslen(temp[c]));
				paraMargin += sz.cy + 7;
		}
		//refill temp array for generation column
		refillTempArray();
		paraMargin = resetMargin;
		if (gMaterialList.cpuGeneration != NULL)
			temp[0] = gMaterialList.cpuGeneration;
		for (c = 0; c < 11; c++) {

			TextOut(hdcInst, 0x21a, paraMargin, temp[c], wcslen(temp[c]));
			paraMargin += sz.cy + 7;
		}
		//refill temp array for model column
		refillTempArray();
		paraMargin = resetMargin;
		if (gMaterialList.cpuModel != NULL)
		temp[0] = gMaterialList.cpuModel;
		for (c = 0; c < 11; c++) {

			TextOut(hdcInst, 0x29f, paraMargin, temp[c], wcslen(temp[c]));
			paraMargin += sz.cy + 7;
		}
		//refill temp array for SIZE column
		refillTempArray();
		paraMargin = resetMargin;
		if (gMaterialList.ramSize != NULL)
			temp[1] = gMaterialList.ramSize;
		if (gMaterialList.gcSize != NULL)
			temp[3] = gMaterialList.gcSize;
		if (gMaterialList.hdSize != NULL)
			temp[4] = gMaterialList.hdSize;
		for (c = 0; c < 11; c++) {

			TextOut(hdcInst, 0x34f, paraMargin, temp[c], wcslen(temp[c]));
			paraMargin += sz.cy + 7;
		}
		//refill temp array for SIZE column
		refillTempArray();
		paraMargin = resetMargin;

		for (c = 0; c < 11; c++) {

			if(prices[c] == 0)
			TextOut(hdcInst, 0x39a, paraMargin, TEXT("-"), 1);
			else
			{
				TextOut(hdcInst, 0x39a, paraMargin, _itow(prices[c], tempPrice, 10), wcslen(_itow(prices[c], tempPrice, 10)));
			}
			paraMargin += sz.cy + 7;
		}
		// price
		for (c = 0; c < 11; c++) {

			totalPrice += prices[c];
		}
		TextOut(hdcInst, 0x39a, paraMargin + sz.cy + 7, TEXT("RS."), 3);
		TextOut(hdcInst, 0x3C0, paraMargin + sz.cy + 7, _itow(totalPrice, tempPrice, 10), wcslen(_itow(totalPrice, tempPrice, 10)));
		//top
		GetClientRect(gHwnd, &rc);
		SelectObject(hdcInst, CreateFont(30, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, ANSI_CHARSET,
			OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE,
			TEXT("Broadway")));
		SetTextColor(hdcInst, RGB(204, 204, 204));
		DrawText(hdcInst, TEXT("------Vivid Computer Shop------"), -1, &rc, DT_SINGLELINE | DT_CENTER);
		//bottom
		SelectObject(hdcInst, CreateFont(20, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, ANSI_CHARSET,
			OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE,
			TEXT("Times New Roman Bold")));
		SetTextColor(hdcInst, RGB(204, 102, 0));
		if(receiptSavedFlag) 
		TextOut(hdcInst, 0x40, 0x220, TEXT("**THANK YOU for shopping with us. Press 'ESC' to exit."), 70);
		else
		TextOut(hdcInst, 0x40, 0x220, TEXT("**Press 'P' to get the recipt or 'ESC' to end shopping"), 55);
	}
	EndPaint(gHwnd, &ps);
	 
	return 0;

}


bool initializeDialogBox(void) {

	int i;

	//Hide sub-selections
	ShowWindow(GetDlgItem(ghDlg, ID_CBCPUPROCESSOR), SW_HIDE);
	ShowWindow(GetDlgItem(ghDlg, ID_CBCPUMODEL), SW_HIDE);
	ShowWindow(GetDlgItem(ghDlg, ID_CBCPUGENERATION), SW_HIDE);
	ShowWindow(GetDlgItem(ghDlg, ID_CBRAMSIZE), SW_HIDE);
	ShowWindow(GetDlgItem(ghDlg, ID_CBGCSIZE), SW_HIDE);
	ShowWindow(GetDlgItem(ghDlg, ID_CBGCSERIAL1), SW_HIDE);
	ShowWindow(GetDlgItem(ghDlg, ID_CBHDSIZE), SW_HIDE);
		//test
	i = sizeof(cpuCompany);
	i = sizeof(intelCoreI7Models);
	//add strings to all comboboxes
	//CPU
	for (i = 0; i < sizeof(cpuCompany) / SIZE_OF_POINTER; i++)
	{
		SendDlgItemMessage(ghDlg, ID_CBCPUCOMPANY, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCSTR)cpuCompany[i]);
	}
	SendDlgItemMessage(ghDlg, ID_CBCPUCOMPANY, CB_SELECTSTRING, (WPARAM)0, (LPARAM)(LPCSTR)cpuCompany[0]);
	//RAM	
	for (i = 0; i < sizeof(RAMName) / SIZE_OF_POINTER; i++)
	{
		SendDlgItemMessage(ghDlg, ID_CBRAM, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCSTR)RAMName[i]);
	}
	SendDlgItemMessage(ghDlg, ID_CBRAM, CB_SELECTSTRING, (WPARAM)0, (LPARAM)(LPCSTR)RAMName[0]);
	//MotherBoard
	for (i = 0; i < sizeof(motherBoard) / SIZE_OF_POINTER; i++)
	{
		SendDlgItemMessage(ghDlg, ID_CBMOTHERBOSRD, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCSTR)motherBoard[i]);
	}
	SendDlgItemMessage(ghDlg, ID_CBMOTHERBOSRD, CB_SELECTSTRING, (WPARAM)0, (LPARAM)(LPCSTR)motherBoard[0]);
	//Graphics Card	
	for (i = 0; i < sizeof(graphicsCard) / SIZE_OF_POINTER; i++)
	{
		SendDlgItemMessage(ghDlg, ID_CBGCCOMPANY, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCSTR)graphicsCard[i]);
	}
	SendDlgItemMessage(ghDlg, ID_CBGCCOMPANY, CB_SELECTSTRING, (WPARAM)0, (LPARAM)(LPCSTR)graphicsCard[0]);
	// Hard disk	
	for (i = 0; i < sizeof(hardDiskCompany) / SIZE_OF_POINTER; i++)
	{
		SendDlgItemMessage(ghDlg, ID_CBHDCOMPANY, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCSTR)hardDiskCompany[i]);
	}
	SendDlgItemMessage(ghDlg, ID_CBHDCOMPANY, CB_SELECTSTRING, (WPARAM)0, (LPARAM)(LPCSTR)hardDiskCompany[0]);
	//CD/DVD Drive
	for (i = 0; i < sizeof(CDDVDDrive) / SIZE_OF_POINTER; i++)
	{
		SendDlgItemMessage(ghDlg, ID_CBCDDVDDRIVE, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCSTR)CDDVDDrive[i]);
	}
	SendDlgItemMessage(ghDlg, ID_CBCDDVDDRIVE, CB_SELECTSTRING, (WPARAM)0, (LPARAM)(LPCSTR)CDDVDDrive[0]);
	//SMPS	
	for (i = 0; i < sizeof(SMPS) / SIZE_OF_POINTER; i++)
	{
		SendDlgItemMessage(ghDlg, ID_CBSMPS, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCSTR)SMPS[i]);
	}
	SendDlgItemMessage(ghDlg, ID_CBSMPS, CB_SELECTSTRING, (WPARAM)0, (LPARAM)(LPCSTR)SMPS[0]);
	//Cabinet	
	for (i = 0; i < sizeof(cabinet) / SIZE_OF_POINTER; i++)
	{
		SendDlgItemMessage(ghDlg, ID_CBCABINET, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCSTR)cabinet[i]);
	}
	SendDlgItemMessage(ghDlg, ID_CBCABINET, CB_SELECTSTRING, (WPARAM)0, (LPARAM)(LPCSTR)cabinet[0]);
	//Keyboard
	for (i = 0; i < sizeof(keyboard) / SIZE_OF_POINTER; i++)
	{
		SendDlgItemMessage(ghDlg, ID_CBKEYBOARD, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCSTR)keyboard[i]);
	}
	SendDlgItemMessage(ghDlg, ID_CBKEYBOARD, CB_SELECTSTRING, (WPARAM)0, (LPARAM)(LPCSTR)keyboard[0]);
	//Mouse	
	for (i = 0; i < sizeof(mouse) / SIZE_OF_POINTER; i++)
	{
		SendDlgItemMessage(ghDlg, ID_CBMOUSECOMPANY, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCSTR)mouse[i]);
	}
	SendDlgItemMessage(ghDlg, ID_CBMOUSECOMPANY, CB_SELECTSTRING, (WPARAM)0, (LPARAM)(LPCSTR)mouse[0]);
	//Monitor
	for (i = 0; i < sizeof(monitor) / SIZE_OF_POINTER; i++)
	{
		SendDlgItemMessage(ghDlg, ID_CBMONITORCOMPANY, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCSTR)monitor[i]);
	}
	SendDlgItemMessage(ghDlg, ID_CBMONITORCOMPANY, CB_SELECTSTRING, (WPARAM)0, (LPARAM)(LPCSTR)monitor[0]);
	
	return true;
}


void enableIntelProcessorCB(void) {
	int j;
	for (j = 0; j < sizeof(intelProcessor) / SIZE_OF_POINTER; j++)
	{
		SendDlgItemMessage(ghDlg, ID_CBCPUPROCESSOR, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCSTR)intelProcessor[j]);
	}
	SendDlgItemMessage(ghDlg, ID_CBCPUPROCESSOR, CB_SELECTSTRING, (WPARAM)0, (LPARAM)(LPCSTR)intelProcessor[0]);
}

void enableAMDProcessorCB(void) {
	int j;
	for (j = 0; j < sizeof(AMDProcessor) / SIZE_OF_POINTER; j++)
	{
		SendDlgItemMessage(ghDlg, ID_CBCPUPROCESSOR, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCSTR)AMDProcessor[j]);
	}
	SendDlgItemMessage(ghDlg, ID_CBCPUPROCESSOR, CB_SELECTSTRING, (WPARAM)0, (LPARAM)(LPCSTR)AMDProcessor[0]);
}

void enableI7ModelCB(void) {
	int j;
	for (j = 0; j < sizeof(intelCoreI7Models) / SIZE_OF_POINTER; j++)
	{
		SendDlgItemMessage(ghDlg, ID_CBCPUMODEL, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCSTR)intelCoreI7Models[j]);
	}
	SendDlgItemMessage(ghDlg, ID_CBCPUMODEL, CB_SELECTSTRING, (WPARAM)0, (LPARAM)(LPCSTR)intelCoreI7Models[0]);
}

void enableI5ModelCB(void) {
	int j;
	for (j = 0; j < sizeof(intelCoreI5Models) / SIZE_OF_POINTER; j++)
	{
		SendDlgItemMessage(ghDlg, ID_CBCPUMODEL, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCSTR)intelCoreI5Models[j]);
	}
	SendDlgItemMessage(ghDlg, ID_CBCPUMODEL, CB_SELECTSTRING, (WPARAM)0, (LPARAM)(LPCSTR)intelCoreI5Models[0]);
}

void enableRyzenModelCB(void) {
	int j;
	for (j = 0; j < sizeof(AMDRyzenModels) / SIZE_OF_POINTER; j++)
	{
		SendDlgItemMessage(ghDlg, ID_CBCPUMODEL, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCSTR)AMDRyzenModels[j]);
	}
	SendDlgItemMessage(ghDlg, ID_CBCPUMODEL, CB_SELECTSTRING, (WPARAM)0, (LPARAM)(LPCSTR)AMDRyzenModels[0]);
}

void enableSempronModelCB(void) {
	int j;
	for (j = 0; j < sizeof(AMDSempronModels) / SIZE_OF_POINTER; j++)
	{
		SendDlgItemMessage(ghDlg, ID_CBCPUMODEL, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCSTR)AMDSempronModels[j]);
	}
	SendDlgItemMessage(ghDlg, ID_CBCPUMODEL, CB_SELECTSTRING, (WPARAM)0, (LPARAM)(LPCSTR)AMDSempronModels[0]);
}

void enableIntelGenCB(void) {
	int j;
	for (j = 0; j < sizeof(intelCpuGenration) / SIZE_OF_POINTER; j++)
	{
		SendDlgItemMessage(ghDlg, ID_CBCPUGENERATION, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCSTR)intelCpuGenration[j]);
	}
	SendDlgItemMessage(ghDlg, ID_CBCPUGENERATION, CB_SELECTSTRING, (WPARAM)0, (LPARAM)(LPCSTR)intelCpuGenration[0]);
}

void enableAMDGenCB(void) {
	int j;
	for (j = 0; j < sizeof(AMDCpuGenration) / SIZE_OF_POINTER; j++)
	{
		SendDlgItemMessage(ghDlg, ID_CBCPUGENERATION, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCSTR)AMDCpuGenration[j]);
	}
	SendDlgItemMessage(ghDlg, ID_CBCPUGENERATION, CB_SELECTSTRING, (WPARAM)0, (LPARAM)(LPCSTR)AMDCpuGenration[0]);
}

void enableRAMSizeCB(void) {
	int j;

	for (j = 0; j < sizeof(RAMSize) / SIZE_OF_POINTER; j++)
	{
		SendDlgItemMessage(ghDlg, ID_CBRAMSIZE, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCSTR)RAMSize[j]);
	}
	SendDlgItemMessage(ghDlg, ID_CBRAMSIZE, CB_SELECTSTRING, (WPARAM)0, (LPARAM)(LPCSTR)RAMSize[0]);
}


void enableIntelGCSerialCB(void) {
	int j;

	for (j = 0; j < sizeof(graphicsIntelSerial) / SIZE_OF_POINTER; j++)
	{
		SendDlgItemMessage(ghDlg, ID_CBGCSERIAL1, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCSTR)graphicsIntelSerial[j]);
	}
	SendDlgItemMessage(ghDlg, ID_CBGCSERIAL1, CB_SELECTSTRING, (WPARAM)0, (LPARAM)(LPCSTR)graphicsIntelSerial[0]);
}

void enableAMDGCSerialCB(void) {
	int j;

	for (j = 0; j < sizeof(graphicsAMDSerial) / SIZE_OF_POINTER; j++)
	{
		SendDlgItemMessage(ghDlg, ID_CBGCSERIAL1, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCSTR)graphicsAMDSerial[j]);
	}
	SendDlgItemMessage(ghDlg, ID_CBGCSERIAL1, CB_SELECTSTRING, (WPARAM)0, (LPARAM)(LPCSTR)graphicsAMDSerial[0]);
}

void enableNVIDIAGCSerialCB(void) {
	int j;

	for (j = 0; j < sizeof(graphicsNVIDIASerial) / SIZE_OF_POINTER; j++)
	{
		SendDlgItemMessage(ghDlg, ID_CBGCSERIAL1, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCSTR)graphicsNVIDIASerial[j]);
	}
	SendDlgItemMessage(ghDlg, ID_CBGCSERIAL1, CB_SELECTSTRING, (WPARAM)0, (LPARAM)(LPCSTR)graphicsNVIDIASerial[0]);
}


void enableGCSizeSerialCB(void) {
	int j;

	for (j = 0; j < sizeof(graphicsCardSize) / SIZE_OF_POINTER; j++)
	{
		SendDlgItemMessage(ghDlg, ID_CBGCSIZE, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCSTR)graphicsCardSize[j]);
	}
	SendDlgItemMessage(ghDlg, ID_CBGCSIZE, CB_SELECTSTRING, (WPARAM)0, (LPARAM)(LPCSTR)graphicsCardSize[0]);
}

void enableHDSizeCB(void) {
	int j;
	for (j = 0; j < sizeof(hardDiskSize) / SIZE_OF_POINTER; j++)
	{
		SendDlgItemMessage(ghDlg, ID_CBHDSIZE, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCSTR)hardDiskSize[j]);
	}
	SendDlgItemMessage(ghDlg, ID_CBHDSIZE, CB_SELECTSTRING, (WPARAM)0, (LPARAM)(LPCSTR)hardDiskSize[0]);
}

void refillTempArray(void) {
	int c;

	for (c = 0; c < 11; c++) {
	temp[c] = TEXT("-");
	}

}

void writeReceiptFile(TCHAR * fileName) {

	void formatStringForFile(TCHAR *buffer, TCHAR *data1, TCHAR *data2, TCHAR *data3, TCHAR *data4, TCHAR *data5, TCHAR *data6, TCHAR *data7); //, TCHAR *data8,
	void writeLineIntoFile(TCHAR data[]);

	TCHAR fName[60];
	ua_wcscpy(fName, fileName);	  
	fp = _wfopen(wcscat(fileName, TEXT("-Receipt.txt")), TEXT("w, ccs=UNICODE"));

	if (fp != NULL) {
		writeLineIntoFile(L"**************************************Vivid Computer Shop*****************************************\n\n\n\n");
		gwInvoiceNumber++;
		wsprintf(str, TEXT("\r  Name : %s                                                     Invoice No : %d \n\n"), fName, gwInvoiceNumber);
		writeLineIntoFile(L"--------------------------------------------------------------------------------------------------\n");
		writeLineIntoFile(str);
		writeLineIntoFile(L"--------------------------------------------------------------------------------------------------\n");
		writeLineIntoFile(fileIOSrings.headingR);
		writeLineIntoFile(L"\n\n");

	if (prices[0] != 0) {
		
		formatStringForFile(fileIOSrings.cpuCompanyR,
			items[0],
			(gMaterialList.cpuCompany != NULL ? gMaterialList.cpuCompany : TEXT("-")),
			(gMaterialList.cpuProcessor != NULL ? gMaterialList.cpuProcessor : TEXT("-")),
			(gMaterialList.cpuGeneration != NULL ? gMaterialList.cpuGeneration : TEXT("-")),
			(gMaterialList.cpuModel != NULL ? gMaterialList.cpuModel : TEXT("-")),
			TEXT("-"),
			_itow(prices[0], tempPrice, 10));
		writeLineIntoFile(fileIOSrings.cpuCompanyR);
		writeLineIntoFile(L"\n\n");
	}

	if (prices[1] != 0) {

		formatStringForFile(fileIOSrings.ramR,
			items[1],
			(gMaterialList.ram != NULL ? gMaterialList.ram : TEXT("-")),
			TEXT("-"), 
			TEXT("-"),
			TEXT("-"),
			(gMaterialList.ramSize != NULL ? gMaterialList.ramSize : TEXT("-")),
			_itow(prices[1], tempPrice, 10));
		writeLineIntoFile(fileIOSrings.ramR);
		writeLineIntoFile(L"\n\n");

	}

	if (prices[2] != 0) {

		formatStringForFile(fileIOSrings.motherBoardR,
			items[2],
			(gMaterialList.motherBoard != NULL ? gMaterialList.motherBoard : TEXT("-")),
			TEXT("-"),
			TEXT("-"),
			TEXT("-"),
			TEXT("-"),
			_itow(prices[2], tempPrice, 10));
		writeLineIntoFile(fileIOSrings.motherBoardR);
		writeLineIntoFile(L"\n\n");

	}

	if (prices[3] != 0) {

		formatStringForFile(fileIOSrings.gcCompanyR,
			items[3],
			(gMaterialList.gcCompany != NULL ? gMaterialList.gcCompany : TEXT("-")),
			(gMaterialList.gcSerial != NULL ? gMaterialList.gcSerial : TEXT("-")),
			TEXT("-"),
			TEXT("-"),
			(gMaterialList.gcSize != NULL ? gMaterialList.gcSize : TEXT("-")),
			_itow(prices[3], tempPrice, 10));
		writeLineIntoFile(fileIOSrings.gcCompanyR);
		writeLineIntoFile(L"\n\n");

	}

	if (prices[4] != 0) {

		formatStringForFile(fileIOSrings.hdCompanyR,
			items[4],
			(gMaterialList.hdCompany != NULL ? gMaterialList.hdCompany : TEXT("-")),
			TEXT("-"),
			TEXT("-"),
			TEXT("-"),
			(gMaterialList.hdSize != NULL ? gMaterialList.hdSize : TEXT("-")),
			_itow(prices[4], tempPrice, 10));
		writeLineIntoFile(fileIOSrings.hdCompanyR);
		writeLineIntoFile(L"\n\n");

	}

	if (prices[5] != 0) {

		formatStringForFile(fileIOSrings.cdDvdDriveR,
			items[5],
			(gMaterialList.cdDvdDrive != NULL ? gMaterialList.cdDvdDrive : TEXT("-")),
			TEXT("-"),
			TEXT("-"),
			TEXT("-"),
			TEXT("-"),
			_itow(prices[5], tempPrice, 10));
		writeLineIntoFile(fileIOSrings.cdDvdDriveR);
		writeLineIntoFile(L"\n\n");

	}

	if (prices[6] != 0) {

		formatStringForFile(fileIOSrings.smpsR,
			items[6],
			(gMaterialList.smps != NULL ? gMaterialList.smps : TEXT("-")),
			TEXT("-"),
			TEXT("-"),
			TEXT("-"),
			TEXT("-"),
			_itow(prices[6], tempPrice, 10));
		writeLineIntoFile(fileIOSrings.smpsR);
		writeLineIntoFile(L"\n\n");
	}

	if (prices[7] != 0) {

		formatStringForFile(fileIOSrings.cabinetR,
			items[7],
			(gMaterialList.cabinet != NULL ? gMaterialList.cabinet : TEXT("-")),
			TEXT("-"),
			TEXT("-"),
			TEXT("-"),
			TEXT("-"),
			_itow(prices[7], tempPrice, 10));
		writeLineIntoFile(fileIOSrings.cabinetR);
		writeLineIntoFile(L"\n\n");
	}

	if (prices[8] != 0) {

		formatStringForFile(fileIOSrings.keyboardR,
			items[8],
			(gMaterialList.keyboard != NULL ? gMaterialList.keyboard : TEXT("-")),
			TEXT("-"),
			TEXT("-"),
			TEXT("-"),
			TEXT("-"),
			_itow(prices[6], tempPrice, 10));
		writeLineIntoFile(fileIOSrings.keyboardR);
		writeLineIntoFile(L"\n\n");
	}

	if (prices[9] != 0) {

		formatStringForFile(fileIOSrings.mouseR,
			items[9],
			(gMaterialList.mouse != NULL ? gMaterialList.mouse : TEXT("-")),
			TEXT("-"),
			TEXT("-"),
			TEXT("-"),
			TEXT("-"),
			_itow(prices[9], tempPrice, 10));
		writeLineIntoFile(fileIOSrings.mouseR);
		writeLineIntoFile(L"\n\n");
	}

	if (prices[10] != 0) {

		formatStringForFile(fileIOSrings.monitorR,
			items[10],
			(gMaterialList.monitor != NULL ? gMaterialList.monitor : TEXT("-")),
			TEXT("-"),
			TEXT("-"),
			TEXT("-"),
			TEXT("-"),
			_itow(prices[10], tempPrice, 10));
		writeLineIntoFile(fileIOSrings.monitorR);
		writeLineIntoFile(L"\n\n");

	}

		writeLineIntoFile(L"-----------------------------------------------------------------------------------------------\n");
		writeLineIntoFile(L"                                              TOTAL | Rs. ");
		writeLineIntoFile(_itow(totalPrice, tempPrice, 10));

	}
	totalPrice = 0;
	_fcloseall();

}

void writeLineIntoFile(TCHAR data[])
{
	gbErrorFlag = fputws(data, fp);
	if (FALSE == gbErrorFlag)
	{
		printf("Failed to write file.\n");
	}
}

void formatStringForFile(TCHAR *buffer, TCHAR *data1, TCHAR *data2, TCHAR *data3, TCHAR *data4, TCHAR *data5, TCHAR *data6, TCHAR *data7)
{
		wcscat(buffer, data1);
		wcscat(buffer, TEXT(" | "));
		wcscat(buffer, data2);
		wcscat(buffer, TEXT(" | "));
		wcscat(buffer, data3);
		wcscat(buffer, TEXT(" | "));
		wcscat(buffer, data4);
		wcscat(buffer, TEXT(" | "));
		wcscat(buffer, data5);
		wcscat(buffer, TEXT(" | "));
		wcscat(buffer, data6);
		wcscat(buffer, TEXT(" | "));
		wcscat(buffer, data7);
}
