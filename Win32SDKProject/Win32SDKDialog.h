// CPU 101 & 102 kept reserved
#define ID_CBCPUCOMPANY 103
#define ID_CBCPUPROCESSOR 104
#define ID_CBCPUMODEL 105
#define ID_CBCPUGENERATION 106
#define ID_CBRAM 107
#define ID_CBRAMSIZE 108
#define ID_CBMOTHERBOSRD 109
#define ID_CBGCCOMPANY 110
#define ID_CBGCSIZE 111
#define ID_CBGCSERIAL 112
#define ID_CBHDCOMPANY 113
#define ID_CBHDSIZE 114
#define ID_CBCDDVDDRIVE 115
#define ID_CBSMPS 116
#define ID_CBCABINET 117
#define ID_CBKEYBOARD 118
#define ID_CBMOUSECOMPANY 119
#define ID_CBMONITORCOMPANY 120
#define ID_CBGCSERIAL1 121
//resources
#define IDB_SPLASHID 200
#define IDB_BGID 201
// macros declaration 
#define WINWIDTH 1400
#define WINHEIGHT 800
#define SIZE_OF_POINTER 4
//callback functions
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
BOOL CALLBACK MyDialogProc(HWND, UINT, WPARAM, LPARAM);
BOOL CALLBACK MyReceiptDialogProc(HWND, UINT, WPARAM, LPARAM);
BOOL CALLBACK MyFileIODialogProc(HWND, UINT, WPARAM, LPARAM);
BOOL CALLBACK MyThankyouProc(HWND, UINT, WPARAM, LPARAM);
//structures
typedef struct userMaterialDetails {
	TCHAR *cpuCompany;
	TCHAR *cpuProcessor;
	TCHAR *cpuModel;
	TCHAR *cpuGeneration;
	TCHAR *ram;
	TCHAR *ramSize;
	TCHAR *motherBoard;
	TCHAR *gcCompany;
	TCHAR *gcSize;
	TCHAR *gcSerial;
	TCHAR *hdCompany;
	TCHAR *hdSize;
	TCHAR *cdDvdDrive;
	TCHAR *smps;
	TCHAR *cabinet;
	TCHAR *keyboard;
	TCHAR *mouse;
	TCHAR *monitor;
}materialList;
materialList gMaterialList;

typedef struct receiptData {
	TCHAR headingR[255];
	TCHAR cpuCompanyR[255];
	TCHAR ramR[255];
	TCHAR motherBoardR[255];
	TCHAR gcCompanyR[255];
	TCHAR hdCompanyR[255];
	TCHAR cdDvdDriveR[255];
	TCHAR smpsR[255];
	TCHAR cabinetR[255];
	TCHAR keyboardR[255];
	TCHAR mouseR[255];
	TCHAR monitorR[255];
}rData;
rData fileIOSrings;

//database
#pragma region HardwareDatabase

#pragma region CPUData
TCHAR *cpuCompany[] = { TEXT("--Select Option--"), TEXT("Intel"), TEXT("AMD") };
TCHAR *intelProcessor[] = { TEXT("--Select Option--"), TEXT("Core-i7"), TEXT("Core-i5") };
TCHAR *AMDProcessor[] = { TEXT("--Select Option--"), TEXT("Ryzen"), TEXT("Sempron") }; // add later

TCHAR *intelCoreI7Models[] = { TEXT("--Select Option--"), TEXT("8700U"), TEXT("8550"), TEXT("7740"), TEXT("7700T"), TEXT("7800"), TEXT("6785") }; // add later
TCHAR *intelCoreI5Models[] = { TEXT("--Select Option--"), TEXT("8600"), TEXT("8400"), TEXT("7640"), TEXT("7600"), TEXT("7500"), TEXT("7400"), TEXT("6585"), TEXT("6400") }; // add later
TCHAR *AMDRyzenModels[] = { TEXT("--Select Option--"), TEXT("Threadripper 1950X"), TEXT("7 Pro 1700X"), TEXT("5 Pro 1600X") }; // add later
TCHAR *AMDSempronModels[] = { TEXT("--Select Option--"),TEXT("3600"), TEXT("3500"), TEXT("3400"), TEXT("3200") }; // add later

TCHAR *intelCpuGenration[] = { TEXT("--Select Option--"), TEXT("Lynnfield"), TEXT("Clarkdale"),TEXT("Sandy Bridge"), TEXT("Ivy Bridge "),TEXT("Hasswell"), TEXT("Broadwell"), TEXT("Skylake"), TEXT("Kaby Lake"), TEXT("Coffee Lake") }; // add later
TCHAR *AMDCpuGenration[] = { TEXT("--Select Option--"),TEXT("Summit Ridge"), TEXT("Thoroughbred"),TEXT("Thorton"), TEXT("Barton"),TEXT("Paris"), TEXT("Palermo"), TEXT("Manila") }; // add later
#pragma endregion

#pragma region RAMData
TCHAR *RAMName[] = { TEXT("--Select Option--"), TEXT("GSkill"), TEXT("ADATA"), TEXT("IBM"), TEXT("Micron Technology") };
TCHAR *RAMSize[] = { TEXT("--Select Option--"),TEXT("8GB"), TEXT("16GB") };
#pragma endregion

#pragma region MotherBoardData
TCHAR *motherBoard[] = { TEXT("--Select Option--"), TEXT("Intel DH61WW"), TEXT("Intel DH61BF"), TEXT("ASUS H81M-CS"), TEXT("Gigabyte GA-Z170X"), TEXT("AMD Gigabyte 990FXA-UD3") };
#pragma endregion

#pragma region GraphicsCardData
TCHAR *graphicsCard[] = { TEXT("--Select Option--"), TEXT("Intel"), TEXT("AMD"), TEXT("NVIDIA") };
TCHAR *graphicsCardSize[] = { TEXT("--Select Option--"), TEXT("4GB"),TEXT("8GB"), TEXT("16GB") };
TCHAR *graphicsIntelSerial[] = { TEXT("--Select Option--"), TEXT("IntelHD4000"), TEXT("IntelHD8000") }; // add later
TCHAR *graphicsAMDSerial[] = { TEXT("--Select Option--"), TEXT("RX Vega 64"),TEXT("R9 Fury X"), TEXT("RX 560"), TEXT("HD 7950") }; // add later
TCHAR *graphicsNVIDIASerial[] = { TEXT("--Select Option--"), TEXT("GTX 770 GTX 590"), TEXT("GTX 1050"), TEXT("GTX 1050 Ti"), TEXT("GTX 960"), TEXT("GTX 950") }; // add later
#pragma endregion

#pragma region HardDiskData
TCHAR *hardDiskCompany[] = { TEXT("--Select Option--"), TEXT("Seagate"), TEXT("WD") }; //add later if required
TCHAR *hardDiskSize[] = { TEXT("--Select Option--"), TEXT("500GB"),TEXT("1TB") };
#pragma endregion

#pragma region CD/DVDDriveData
TCHAR *CDDVDDrive[] = { TEXT("--Select Option--"), TEXT("Samsung DVD-RW 22X SATA DL") }; //add later if required
#pragma endregion

#pragma region SMPS
TCHAR *SMPS[] = { TEXT("--Select Option--"), TEXT("Corsair AX-860H") }; //add later if required
#pragma endregion

#pragma region CabinetData
TCHAR *cabinet[] = { TEXT("--Select Option--"), TEXT("iBall"), TEXT("Zebronics"), TEXT("Corsair") }; //add later if required
#pragma endregion

#pragma region KeyboardData
TCHAR *keyboard[] = { TEXT("--Select Option--"), TEXT("Microsoft"), TEXT("Logitek"), TEXT("Lenovo") }; //add later if required
#pragma endregion

#pragma region MouseData
TCHAR *mouse[] = { TEXT("--Select Option--"), TEXT("Microsoft"), TEXT("Logitek"), TEXT("Lenovo") }; //add later if required
#pragma endregion

#pragma region MonitorData
TCHAR *monitor[] = { TEXT("--Select Option--"), TEXT("BenQ PD3200U 4K"), TEXT("AOC Agon AG352UCG"), TEXT("Asus MG248Q"), TEXT("LG 34UC79G-B") }; //add later if required
#pragma endregion
#pragma endregion

//variables
TCHAR *headings[] = { TEXT("DEVICE"), TEXT("COMPANY"), TEXT("SUBTYPE"), TEXT("GENERATION"), TEXT("MODEL"), TEXT("SIZE"), TEXT("TOTAL") };
TCHAR *items[] = { TEXT("01]  CPU"),
TEXT("02]  RAM"),
TEXT("03]  Motherboard"),
TEXT("04]  Graphics Card"),
TEXT("05]  Hard disks"),
TEXT("06]  CD/DVD Drive"),
TEXT("07]  SMPS"),
TEXT("08]  Cabinet"),
TEXT("09]  Keyboard"),
TEXT("10]  Mouse"),
TEXT("11]  Monitor") };
TCHAR *temp[] = { TEXT("-"),
TEXT("-"),
TEXT("-"),
TEXT("-"),
TEXT("-"),
TEXT("-"),
TEXT("-"),
TEXT("-"),
TEXT("-"),
TEXT("-"),
TEXT("-") };


RECT rc;
HINSTANCE hInst;
HWND ghDlg = NULL, gHwnd;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
TCHAR name[50];
TCHAR str[255];
TCHAR tempPrice[20];
HANDLE hFp = NULL;
FILE *fp;
DWORD gdwBytesWritten = 0;
BOOL gbErrorFlag = FALSE;
BOOL receiptSavedFlag = FALSE;
static WORD gwInvoiceNumber = 1200;
static HBITMAP hBitmap;
bool splashScreenActive = true;
int giSelect;
int counter = 0;
int totalPrice;
int prices[11];
int i = 0, X = 0, Y = 0;
